

export class NT{
    public static protoUrl : string[] = ["Client.proto"];
    public static protoName : string[] =["PK5Client"];
    //0 : json , 1 : protobuf , 2 :json 
    public static MSGTYPE  = 1;
    
    public static STATE = {
        Connect:     1,
        AuthConnect:   2,
        AuthConnectRes:  3,
        GetBetRange: 100 ,
        GetBetRangeRes: 101 ,
        SetBet: 102,
        SetBetRes: 103,
        GetMachineList: 104,
        GetMachineListRes: 105,
        GetMachineDetial: 106,
        GetMachineDetialRes: 107,
        IntoGame: 108,
        IntoGameRes: 109,
        Bet: 110,
        BetRes: 111,
        BetDouble: 112,
        BetDoubleRes: 113,
        BetGiveUp: 114,
        BetGiveUpRes: 115,
        LeaveMachine: 116,
        LeaveMachineRes: 117,
        Error: 900
    };

    public  static Config = [
        {
            TYPE:"LOCAL",
            HOST: '',
            PORT : -1,
            isSSL : false
        },{
            TYPE:"PK5Client",
            // HOST: "52.76.110.236",
            //HOST: "127.0.0.1",
            HOST: "54.251.122.228",
            // PORT : 20090,
            PORT : 80,
            isSSL : false
        }
    ];
    static spritePath: any;
    static path: any;
    static spriteName: any;
    static spriteNum: any;
    static uid: any;
    static money: any;
    static userName: any;
    static version: any;
    static jpList: any;
    static huluNum: any = 0;



    static ERROR_CODE={
        1: "Error Input ",
        2: "Account Or Password Error",
        3: "Setting Bet Range Error",
        4: "Get Machines Error",
        5: "Get Machine Detail Error",
        6: "Into Machine Error",
        7: "Bet Error",
        8: "Doble Bet Error",
        9: "Bet Give Up Error"
    }
}

// export NT;