/**
 *
 * @author  ray kao
 * interface for data translate 
 *
 */

interface DataInterface {
   
    type : string ;
    state: number;
    obj : string ;
    objKey : number ;
    data: any;
    resp: boolean;
    respType: number;
    getSendDataTemp?: ()=> DataInterface;
    
}
interface modelRegistObjs {
    type : string ;
    obj : string ;
    state ?: number;
    model: RootData;
}

interface RootInterface{
    type : string;
    obj: string;
    respType: number;
    objKey : number;
    respObj : string ;
    state : number;
    setData:(data:any )=> void;
}

