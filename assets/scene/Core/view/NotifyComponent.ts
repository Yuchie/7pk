import {Controller} from "../controller/Controller";
import {core} from "../Core";
const {ccclass, property} = cc._decorator;

//send data 格式
interface SendData{
    key :string ;
    objKey: number;
    data:any;
}

@ccclass
export class NotifyComponent extends cc.Component {

    @property(Number)
    state: number = 1;


    private compomentHandel = null;
    constructor(){
        super();
    }

    regist(){
        console.log("regist NotifyCompoment state: "+ this.state )
        core.getInstance().registView(this.state, this);
        
    }

    closeConnect(serverType : number ){
        core.getInstance().close(serverType);
    }

    sendToServer(serverType : number ,  param:SendData){
        core.getInstance().send( serverType  , param);
    }

    onLoad(){
        console.log("onload in parent")
        this.regist();
    }
    protected onDestroy(): void {
        console.log("Destroy NotifyCompoment")
        core.getInstance().unregistView(this.state)
    }

    // protected subOnload(){

    // }

    notify(data:any){
        console.log(this);
        console.log(data);
        throw("need to override");
    }
}
