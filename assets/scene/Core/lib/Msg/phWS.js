

var messageFactory = (function () {
    /*
    *	protoFileContents : .proto content
    */
   var self;
   var root;
    function messageFactory(protoFileContents) {
        self = this;
        // console.log(protoFileContents)
        protobuf.load(protoFileContents, function(err ,rootSource){
            // console.log(err);
            // console.log(root);
            root= rootSource;
            // console.log("this.root", this.root);
        });
    }
    /*
    *	get factory
    */
    messageFactory.prototype.getFactory = function (serverType, msgKey) {
        // console.log(msgKey);
        // console.log(serverType);
        // console.log(root);
        // console.log(self);

        return root[serverType]['nested'][msgKey];
    };
    return messageFactory;
})();
var MessageAdapter = (function () {
    self = null;
    //protoFileContents 是 proto 的下載位置 注意 ！！！！
    function MessageAdapter(protoFileContents, MSGDATA, blockList) {
        //if message  chnage to the defferent  structure change the factory
        self = this;
        self.MSGDATA = MSGDATA;
        self.isBlock = false;
        self.blockList = blockList;
        self.factory = new messageFactory(protoFileContents);

        
    }
    /*
    *	endcode the message
    *	msg structure :
    *	{
    *		type: '',
    *		data: {
    *
    *		}
    *	}
    */
   
    MessageAdapter.prototype.setMsg = function (serverType, param) {
        var par = [];
        // console.log(serverType);
        var serverTypeKey = serverType[0];
        // if(param.objKey >=100){
        //     serverTypeKey = serverType[1];

        // }
        console.log("phws.js : =",serverType, param)
        var messageObj = this.factory.getFactory(serverTypeKey, param.key);
        //var msgObj = new messageObj();
        
        var keybuff = new Uint8Array(4);
        keybuff[0] = param.objKey;

        var errMsg =messageObj.verify(param)
        if(errMsg){
            throw Error(errMsg)
        }
        // console.log("param:",param);
        var arrayBufferData = messageObj.create(param.data);
        
        var arrayBuff= messageObj.encode(arrayBufferData).finish();


        // console.log("byte length : ",keybuff.byteLength , arrayBuff.byteLength)
        var temp = new Uint8Array(keybuff.byteLength + arrayBuff.byteLength);
        // console.log(temp)
        temp.set(new Uint8Array(keybuff), 0);
        
        temp.set(new Uint8Array(arrayBuff), keybuff.byteLength);
        // console.log(param.key);
       
        
        
       // return msgObj.toArrayBuffer();
       return temp.buffer;
    };
    /*
    *	decode the message
    *	msg structure :
    *	{
    *		type: '',
    *		data: {
    *
    *		}
    *	}
    */
  
    MessageAdapter.prototype.getMsg = function (serverType, msg) {
        
       
        var keys = new Uint8Array(msg.slice(0,4));
       // var key = keys[0];
       var dataView = new DataView(keys.buffer);

   
       
       var view = new Uint8Array(dataView.buffer, dataView.byteOffset, dataView.byteLength);
       //返回实际数据，前4字节为约定数据，所以从索引4开始返回
       //var data = view.subarray(6, dataView.byteLength);

       //解析消息id
       var msgId = 0;
       for (var i = 0; i < 4; i++) {
           msgId |= view[i ] << (8 * (i));
       }
        var key = msgId;
        var keyStr = self.MSGDATA[key];

        // console.log(serverType);
        var msgDecode = this.factory.getFactory(serverType[0], keyStr);
       
        // console.log(msg)
        // console.log(msg.slice(4, msg.byteLength))
        var arrBuf= msg.slice(4, msg.byteLength);
        var msgobj = msgDecode.decode(new Uint8Array( arrBuf ,0 , arrBuf.byteLength ));
        
        msgobj.key = key;
       
        return msgobj;
    };
    MessageAdapter.prototype.setBlock = function (isBlock) {
        self.isBlock = isBlock;
    };
    return MessageAdapter;
})();
var phWS = {};
(function (phWS) {
    var message = (function () {
        function message(protoFileContent, m, bklist) {
            this.messageBuilder = new MessageAdapter(protoFileContent, m, bklist);
        }
        message.prototype.getBuilder = function () {
            return this.messageBuilder;
        };
        //for get mesg
        message.prototype.getMsg = function (serverType, param) {
            return this.messageBuilder.getMsg(serverType, param);
        };
        //for send 
        message.prototype.setMsg = function (serverType, param) {
           
            return this.messageBuilder.setMsg(serverType, param);
        };
        message.prototype.setBlock = function (isBlock) {
            this.messageBuilder.setBlock(isBlock);
        };
        return message;
    })();
    var websocketPh = (function () {
        var SOCKET_READY = 0;
        var SOCKET_ONCONNECT = 1;
        var SOCEKT_ONMSG = 2;
        var SOCKET_WAITRESP = 3;
        var SOCKET_CLOSE = 4;
        var SOCKET_CONNECTED = 5;
        var SOCKET_ONSEND = 6;
        var SOCKET_ONERROR = 7;
        var MSG_ONCONNECT = 'onConnect';
        var MSG_LOGININPUT = 'LoginInput';
        var self = null;
        function websocketPh(_config, key, protoFileContent) {
            console.log("in the constructure");
            this.id = key;
            this._config = _config;
            this.state = SOCKET_READY;
            this.msgState = MSG_ONCONNECT;
            self = this;
            self.messageBuilder = new MessageAdapter(_config.serverType, protoFileContent);
            self.socktConn = null;
        }
        websocketPh.prototype.init = function () {
            console.log("here is init");
            self.socktConn = new WebSocket("ws://" + this._config.host + ":" + this._config.port, self.id);
            self.socktConn.parent = this;
            self.state = SOCKET_ONCONNECT;
            self.socktConn.binaryType = "arraybuffer"; // We are talking binary
            self.socktConn.onopen = self.onopen;
            self.socktConn.onmessage = self.onmessage;
            self.socktConn.onclose = self.onclose;
        };
        websocketPh.prototype.changeState = function (state) {
            this.state = state;
        };
        websocketPh.prototype.onopen = function (e) {
            this.parent.onOpenCallback("OK");
            this.parent.changeState(SOCKET_CONNECTED);
        };
        websocketPh.prototype.onmessage = function (e) {
            var msg = this.parent.messageBuilder.getMsg(e);
            this.parent.onMessageCallback(msg);
            this.parent.changeState(SOCEKT_ONMSG);
        };
        websocketPh.prototype.onclose = function (e) {
            this.parent.onColoseCallback("close");
        };
        websocketPh.prototype.onerror = function (e) {
            this.parent.changeState(SOCKET_ONERROR);
            throw (e);
        };
        websocketPh.prototype.send = function (param) {
            var msg = self.messageBuilder.setMsg(param);
            self.socktConn.send(msg);
            self.changeState(SOCKET_ONSEND);
        };
        websocketPh.prototype.setOnOpen = function (callback) {
            this.onOpenCallback = callback;
        };
        websocketPh.prototype.setOnMessage = function (callback) {
            this.onMessageCallback = callback;
        };
        websocketPh.prototype.setOnClose = function (callback) {
            this.onColoseCallback = callback;
        };
        websocketPh.prototype.getState = function () {
            return this.state;
        };
        return websocketPh;
    })();
    console.log(websocketPh);
    phWS.websocketPh = websocketPh;
    phWS.Message = message;
})(phWS);

window.phWS =phWS;

if(!window.isMute){
    window.isMute = "0";
}

window.getMute = function( flag){
    window.isMute = flag;
}