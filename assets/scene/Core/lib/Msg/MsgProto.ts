
import {Message} from "./Message";
import {NT} from "../../../constant/constant";

export  class  MsgProto implements Message {

    public static msgProto:MsgProto;

    public  MsgObj = null;
    private protobufPackage : any[]=[];

    public static readyDecode = false;

    constructor(  ){
        
    }

     init( ){
        console.log("init")
        this.protobufPackage = NT.protoName;
        
        var MSG =[];
        for(var k in NT.STATE ){
            MSG[NT.STATE[k]] = k;
        }

        // cc.resources.load( NT.protoUrl ,(e , res)=>{
        //     console.log(res);
        //     var arr = [];
        //     for(var a=0,len =res.length ;a<len ;a++ ){

        //         arr.push(res[a].text);
        //     }
        //     this.MsgObj = new window.phWS.Message( NT.protoUrl ,MSG);  
        //     MsgProto.readyDecode =true;
        //     console.log(this.MsgObj);
        // } );
        this.MsgObj = new window.phWS.Message( NT.protoUrl ,MSG);  
        MsgProto.readyDecode =true;
        console.log("init 2")
    }

    encdoe ( msg : any) {
        
        var encodeData =this.MsgObj.setMsg(this.protobufPackage , msg );
        
        return encodeData;
    }

    decode( msg : any){
     
        var data = this.MsgObj.getMsg(this.protobufPackage , msg );
      
        return data;
    }

    public static getInstance() :MsgProto{

        if(MsgProto.msgProto === null || MsgProto.msgProto ===undefined ){

            MsgProto.msgProto = new MsgProto();
        }

        return MsgProto.msgProto;

    }
    
}
