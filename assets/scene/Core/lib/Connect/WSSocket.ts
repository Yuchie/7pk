export enum SOCKETSTATE{
    CLOSE =0,
    CONNECT =1,
    ERROR =2
}

export enum Event{
    OPEN = 0,
    ONMESSAGE= 1,
    CLOSE= 2,
    ERROR =3
}
import {Connect} from "./Connect";

export class WSSocket implements Connect{
    
    public static Event={
        OPEN : 0,
        ONMESSAGE: 1,
        CLOSE: 2,
        ERROR: 3
    }
    public static SOCKETSTATE={
        CLOSE :0,
        CONNECT :1,
        ERROR :2
    }

    private eventCall: Function[] = [];
    private eventCallObj: Object[] = [];

    private url : string ="";
    private prefix : string ="";
    private state:SOCKETSTATE= SOCKETSTATE.CLOSE;
    private socket : WebSocket;

    private timeoutObj = null;

    constructor (  ){
        
    }
    
    public init (url:string ,isSSH?:boolean){
        if(this.timeoutObj){
            clearTimeout(this.timeoutObj);
        }
        this.url = url;
        if(isSSH){
            this.prefix = "wss://";

        }
        else{
            this.prefix = "ws://";
        }

        this.socket = new WebSocket(this.prefix+this.url+"/websocket");
        this.socket.binaryType = "arraybuffer";
        this.socket.onopen = this.onopen.bind(this);
        this.socket.onmessage = this.onmessage.bind(this);
        this.socket.onclose =this.onclose.bind(this);
        this.socket.onerror =this.onerror.bind(this);
        console.log(this.socket);

    }   

    public  send( msg){
        
        
            this.socket.send(msg);
        
       
        
        
    }

    private onopen(){

        this.state = SOCKETSTATE.CONNECT;
        this.timeoutObj = setInterval(()=>{

            this.send("ping");
        },15000)
    }


    private onmessage(msg){
        console.log("onmessage", msg);
        this.eventCall[Event.ONMESSAGE].call( this.eventCallObj[Event.ONMESSAGE] , msg.data);

        

    }

    private onclose(e){
        console.log("onclose wssocker" ,e);
        console.log("onclose wssocker================" ,this.eventCallObj[Event.CLOSE]);
       
        this.state = SOCKETSTATE.CLOSE;
        this.eventCall[Event.CLOSE].call( this.eventCallObj[Event.CLOSE] , e);
    }

    private onerror(e){
        console.log("socker error ws")
        console.log("socker error ws======", Event.ERROR)
        this.state = SOCKETSTATE.ERROR;
        this.eventCall[Event.ERROR].call( this.eventCallObj[Event.ERROR] , e);
        
    }

    public addEventListener (type: number , fn: Function, bindObj:Object){
        console.log("add event listent socket")
        this.eventCall[type] = fn;
        this.eventCallObj[type] = bindObj;
    }

    public close (){
        this.socket.close();
    }
}