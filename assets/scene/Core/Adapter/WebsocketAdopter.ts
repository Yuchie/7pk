import {Connect} from "../lib/Connect/Connect";
import {WSSocket}  from '../lib/Connect/WSSocket';
import {HTTPConnect} from '../lib/Connect/HttpConnect';

enum SOCKETTYPE{
    SOCKET =0,
    WEBSOCKET = 1,
    HTTP = 2
}

export class NetWork  {
    public static EVENT={
        OPEN : 0,
        ONMESSAGE: 1,
        CLOSE: 2,
        ERROR: 3
    }
    public static SOCKETTYPE = {
        SOCKET :0,
        WEBSOCKET : 1,
        HTTP : 2
    };

    public static getConnect( type: SOCKETTYPE  ):Connect{
        if(type== SOCKETTYPE.WEBSOCKET ){
            return new WSSocket();
        }
        else if(type == SOCKETTYPE.HTTP ){
            return new HTTPConnect();
        }
        
    }   
}
