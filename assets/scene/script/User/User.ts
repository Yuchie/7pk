const {ccclass, property} = cc._decorator;

@ccclass
export default class User extends cc.Component {
    public static money: number = 0;
    public static uid: any = null;;
    public static userName: any = null;


    public static initData(data: any) {
        this.setUserMoney(data.money);
        this.setUid(data.uid);
        this.setUserName(data.userName);
    } 

    public static setUserMoney(money: number) {
        this.money = money;
    }

    public static getUserMoney() {
        return this.money;
    }

    public static setUid(uid: any) {
        this.uid = uid;
    }

    public static getUid() {
        return this.uid;
    }

    public static setUserName(userName: string) {
        this.userName = userName;
    }

    public static getUserName() {
        return this.userName;
    }

}
