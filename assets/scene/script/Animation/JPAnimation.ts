import Game from "../Game/Game";
import { GameSet } from "../Game/GameTypeSet";
import User from "../User/User";
import { GAME_EVENTS, MINI_GAME_EVENTS } from "../Utils/Event";
import { Utils } from "../Utils/Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class JPAnimation extends cc.Component {


    @property(cc.Sprite)
    spriteJP: cc.Sprite = null;

    @property(cc.Label)
    labelJP: cc.Label = null;

    start () {
        this.reset();
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(GAME_EVENTS.SHOW_JACKPOT, (winGold) =>{
            this.showJPAnimation(winGold);
        })
    }

    showJPAnimation(winGold: number){
        this.spriteJP.node.active = true;
        this.labelJP.node.active = true;
        let fakeMoney = 0;
        this.labelJP.string = winGold.toString();
        let gold = {gold: winGold};
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_USER_GOLD, winGold);
        cc.tween(gold)
        .delay(1.5)
        .to(5, {gold: 0}, {
            progress: (start, end, current, ratio) => {
                let num = Math.round(start - (start * ratio));
                this.labelJP.string = String(num);
                fakeMoney = User.getUserMoney() - num;
                cc.find("Canvas").getComponent(Game).setFakerUserMoney(fakeMoney);
                return num;
            }
        })
        .delay(2)
        .call(()=> {
            this.reset();
            cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.StartMiniGame);

        })
        .start()
    }

    reset() {
        this.spriteJP.node.active = false;
        this.labelJP.node.active = false;
    }


}
