import { BaseClass } from "../BaseClass";
import ButtonPannl from "../Game/Button/ButtonPannel";
import { GameSet } from "../Game/GameTypeSet";
import { DataManager } from "../Utils/DataManager";
import { ANI_EVENTS, GAME_EVENTS, JP_EVENT, MINI_GAME_EVENTS } from "../Utils/Event";
import LabelAnimation from "./LabelAnimation";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AnimationPannel extends cc.Component {

    @property(cc.Sprite)
    spriteJPBar: cc.Sprite = null;

    @property(cc.Sprite)
    spriteJPNum: cc.Sprite = null;

    @property(cc.Node)
    nodeFruit: cc.Node[] = [];

    @property(cc.SpriteFrame)
    frameBar: cc.SpriteFrame[] = [];

    @property([LabelAnimation])
    animationLabel: LabelAnimation[] = [];

    @property(ButtonPannl)
    buttonScript: ButtonPannl = null;

    public get normalWinIndex() {
        return AnimationPannel.normalWin;
    }

    public set normalWinIndex(value) {
        AnimationPannel.normalWin = value;
    }

    public get jpWinIndex() {
        return AnimationPannel.jpWin;
    }

    public set jpWinIndex(value) {
        AnimationPannel.jpWin = value;
    }

    public static normalWin = 0;
    public static jpWin = 0;
    private startNum: number = 0;
    private round = 0 ;
    public tweenBar: cc.Tween = null;
    public index = 0;
    public endIndex = 0;


    start () {
        this.tweenBar = cc.tween(this.spriteJPBar.node);
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(MINI_GAME_EVENTS.PLAY_BLINK_MINI, ()=> {
            this.registerNode();
            this.playBlinkNormalWinAni();
        });
        cc.systemEvent.on(JP_EVENT.PLAY_BLINK_JP, () => {
            this.registerNode();
            this.playBlinkJPWinAni();
        });
        cc.systemEvent.on(MINI_GAME_EVENTS.STOP_BLINK_MINI, ()=>{
            this.registerNode(); 
            this.stopBlinkNormalWinAni();
        });
        cc.systemEvent.on(JP_EVENT.STOP_BLINK_JP, ()=>{ 
            this.registerNode();
            this.stopBlinkJPWinAni();
        });
        cc.systemEvent.on(GAME_EVENTS.UPDATE_NORMAL_WIN_INDEX, (value) => {
                AnimationPannel.normalWin = value;
                console.log("UPDATE_NORMAL_WIN_INDEX", AnimationPannel.normalWin);
        });
        cc.systemEvent.on(GAME_EVENTS.UPDATE_JP_WIN_INDEX, (value) => {
            AnimationPannel.jpWin = value;
        });

        cc.systemEvent.on(GAME_EVENTS.UPDATE_WIN_LABEL, (value) => {
            this.registerNode();
            this.setWinLabel(value, false);
        })

        cc.systemEvent.on(ANI_EVENTS.RUN_LABEL_TWEEN, (bool) => {
            console.log("RUN_LABEL_TWEEN=======");
            this.runWinLabel(bool);
        })


    }

    registerNode() {
        if(this.nodeFruit == null){
            let fruits = [];
            let fruit = cc.find("Canvas/WinFruit/WinLabels").children;
            fruit.forEach((element) => {
                fruits.push(element);
            })
            this.nodeFruit = fruits;
        }

        if(this.spriteJPBar == null) {
            this.spriteJPBar = cc.find("Canvas/WinFruit/JPBar").getComponent(cc.Sprite);
        }

        if(this.spriteJPNum == null) {
            this.spriteJPNum = cc.find("Canvas/WinFruit/JPSprite").getComponent(cc.Sprite);
        }
        // if(this.frameBar == null) {
            
        // }
        if(this.animationLabel == null) {
            let array = [];
            let children = cc.find("Canvas/WinFruit/WinLabels").children;
            children.forEach((element) => {
                array.push(element.getChildByName("Label").getComponent(LabelAnimation));
            })
            this.animationLabel = array;
        }

        if(this.buttonScript == null) {
            this.buttonScript = cc.find("Canvas/Buttons").getComponent(ButtonPannl);
            
        }
    }

    playBlinkNormalWinAni() {
       let index = BaseClass.jpListResult(AnimationPannel.normalWin);
       console.log("playblinkNOrmalWInANi=======",  this.nodeFruit[index]);
       cc.tween(this.nodeFruit[index])
       .delay(0.4)
       .call(()=>{this.nodeFruit[index].opacity = 255;})
       .delay(0.4)
       .call(()=>{this.nodeFruit[index].opacity = 0;})
       .union()
       .repeatForever()
       .start()
    }

    stopBlinkNormalWinAni() {
        let index = BaseClass.jpListResult(AnimationPannel.normalWin);
        this.nodeFruit[index].stopAllActions();
        this.nodeFruit[index].opacity = 255;
    }

    playBlinkJPWinAni() {
        let index = BaseClass.jpListResult(AnimationPannel.jpWin);
        cc.tween(this.nodeFruit[index])
        .delay(0.4)
        .call(()=>{this.nodeFruit[index].opacity = 255;})
        .delay(0.4)
        .call(()=>{this.nodeFruit[index].opacity = 0;})
        .union()
        .repeatForever()
        .start()
    }

    stopBlinkJPWinAni() {
        let jpIndex = BaseClass.jpListResult(AnimationPannel.jpWin);
        this.animationLabel[jpIndex].node.stopAllActions();
        this.animationLabel[jpIndex].node.active = true;
    }

    setWinLabel(winGold: number, isMiniGameWin: boolean){
        console.log("===animationpannel", winGold);
        let index = BaseClass.jpListResult(AnimationPannel.normalWin);
        if(isMiniGameWin){
            let gold = this.animationLabel[index].getWinResult() + winGold;
            this.animationLabel[index].setWinResult(gold);
        }else{
            this.animationLabel[index].setWinResult(winGold);
        }
        
        // this.animationLabel[this.index].setWinResult(winGold);
    }

    runWinLabel(bonusEnd: any = false){
        let index = BaseClass.jpListResult(AnimationPannel.normalWin);
        this.buttonScript.setButtonByStatus(GameSet.GameStatus.EndMiniGame);
        console.log("runWinLabel==========count====");
        console.log("runWinLabel==========", index);
        // if(index < 4 && bonusEnd){
        //     cc.systemEvent.emit(GAME_EVENTS.BONUS_WIN, index);
        // }
        // else if(index == 4 && DataManager.getFHBonusWin(DataManager.pokerResult) && bonusEnd){
        //     cc.systemEvent.emit(GAME_EVENTS.BONUS_WIN, index);
        // }else{
            this.animationLabel[index].resultNumRunning();
        // }
    }

    playBarAnimation(start: number, end: number, callback: any){
        this.tweenBar?.stop();
        if(BaseClass.jpListResult(AnimationPannel.normalWin) == 0){
            this.startNum = this.nodeFruit.length - 1;
        }else{
            this.startNum = BaseClass.jpListResult(AnimationPannel.normalWin);
        }
        this.endIndex = end;
        this.spriteJPBar.node.active = true;
        this.updatePosition();

        this.tweenBar
        .delay(1)
        .call(() => {
            if(this.startNum <= 0){
                this.updatePosition();
                if(BaseClass.jpListResult(AnimationPannel.normalWin) == 0){
                    this.startNum = this.nodeFruit.length - 1;
                }else{
                    this.startNum = BaseClass.jpListResult(AnimationPannel.normalWin);
                }
                this.round++;
            }else{
                this.updatePosition();
            }
            cc.log('startNum', this.startNum);
            // console.log(this.startNum, );
            this.startNum--;
        })
        .union()
        .repeat(99999)
        .start();

    }

    updatePosition(){
        this.spriteJPBar.node.y = this.nodeFruit[this.startNum].getPosition().y;
    }

    resetAllColor(){
        this.nodeFruit.forEach(element => {
            element.color = cc.color(255, 255, 255);
        })
    }

    updateSpriteFrameByIndex(){
        if(this.startNum ==1 || this.startNum == 9){
            this.spriteJPBar.spriteFrame = this.frameBar[1];
        }else{
            this.spriteJPBar.spriteFrame = this.frameBar[0];
        }
    }

    stopBarAnimation(){
        this.tweenBar?.stop();
    }

    reset() {
        this.animationLabel.forEach(label => {
            label.reset();
        })
        this.tweenBar?.stop();
        this.index = 0;
        this.endIndex = 0;
        this.startNum = 0;
    }

    resetJP(){
        this.round = 0;
        this.spriteJPBar.node.y = this.nodeFruit[0].getPosition().y;
        this.spriteJPBar.node.active = false;
        this.spriteJPNum.node.y = this.nodeFruit[0].getPosition().y;
        this.spriteJPNum.node.active = false;
        //this.nodeFruit.forEach(fruit => fruit.color = cc.color(255, 255, 255));
    }

}
