import { NT } from "../../constant/constant";
import { BaseClass } from "../BaseClass";
import Game from "../Game/Game";
import JPBetList from "../Game/JP/JPBetList";
import User from "../User/User";
import { GAME_EVENTS, MINI_GAME_EVENTS } from "../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LabelBonusAnimation extends cc.Component {
    @property([cc.Label])
    labelBonus: cc.Label[] = [];

    start () {
        this.registerEvents();
    }

    registerEvents(){
        cc.systemEvent.on(GAME_EVENTS.BONUS_WIN, (index)=>{
            console.log("LabelBonusAnimation=======", index);
            this.checkBonus(index);
        });
    }

    checkBonus(index){
        console.log(JPBetList.bonusArray);
        this.playBonus(JPBetList.bonusArray[index], index);
    }

    playBonus(number: number, index: number) {
        console.log("str number", number);
        let result = {result: number};
        cc.tween(result)
        .to(3, {result: 0}, {
            progress: (start, end, current, ratio) => {
                let num = Math.round(start - (start * ratio));
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_BONUS_NUMBER, index, num);
                return num;
            }
        })
        .call(() =>{
            cc.systemEvent.emit(MINI_GAME_EVENTS.MINIGAME_END, null, false);
            //bonus動畫結束
        })
        .start()
    }


}
