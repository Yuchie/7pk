import { NT } from "../../constant/constant";
import Game from "../Game/Game";
import User from "../User/User";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LabelAnimation extends cc.Component {

    public resultNumber: number = 0;
    public tweenResult: cc.Tween = null;
    public result = null;

    start () {
    }

    registerEvents(){
        // cc.systemEvent.on(MINI_GAME_EVENTS.PLAY_BLINK_MINI, this.playBlinkNormalWinAni);
        // cc.systemEvent.on(JP_EVENT.PLAY_BLINK_JP, this.playBlinkJPWinAni);
    }

    setWinResult(num: number){
        this.resultNumber = num;
        this.node.getComponent(cc.Label).string = String(this.resultNumber);
    }

    getWinResult(){
        return this.resultNumber;
    }

    playBlink() {
        cc.tween(this.node.getComponent(cc.Label).node)
        .delay(0.15)
        .call(()=>{this.node.getComponent(cc.Label).node.active = true})
        .delay(0.15)
        .call(()=>{this.node.getComponent(cc.Label).node.active = false})
        .repeatForever()
    }

    stopBlink() {
        this.node.getComponent(cc.Label).node.stopAllActions();
        this.node.getComponent(cc.Label).node.active = true;
    }

    calculateTime(){
        let time = 0;
        let str = this.resultNumber.toString();
        if(str.length > 4){
            return time = 60;
        }else if(str.length == 4){
            return time = 30;
        }else if (str.length == 3){
            return time = 15;
        }else{
            return time = 10;
        }
    }
    

    resultNumRunning(){
        console.log("resultNumRunning=========")
        let fakeMoney = 0;
        this.result = {result: this.resultNumber};
        this.tweenResult = cc.tween(this.result);
        this.tweenResult
       // .to(this.calculateTime(), {result: 0}, {
        .to(3, {result: 0}, {
            progress: (start, end, current, ratio) => {
                let num = Math.round(start - (start * ratio));
                this.node.getComponent(cc.Label).string = String(num);
                fakeMoney = User.getUserMoney() - num;
                cc.find("Canvas").getComponent(Game).setFakerUserMoney(fakeMoney);
                return num;
            }
        }).call(()=>{
            cc.find("Canvas").getComponent(Game).updateUserMoneylabel();
        }).delay(1)
        .call(()=>{
            this.reset();
            // this.reset();
            cc.find('Canvas').getComponent(Game).endMiniGame();
        })
        .start()
    }

    reset() {
        this.tweenResult?.stop();
        this.resultNumber = 0;
        this.result = null;
    }

}
