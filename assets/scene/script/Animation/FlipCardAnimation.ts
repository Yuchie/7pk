import ImageContainer from "../ImageContainer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FlipCardAnimation extends cc.Component {
    @property(cc.Sprite)
    spriteCard: cc.Sprite = null;

    private payoutCard: any;
    private tweenCard: cc.Tween<cc.Node> = null;
    private isInit = false;
    private isPlayed = false;

    start() {
        this.reset();
        this.init();
    }

    init() {
        if (this.isInit)
            return;
        this.tweenCard = cc.tween(this.spriteCard.node);
        this.isInit = true;
    }

    async setCard(card: any) {
        this.init();
        if (this.isPlayed && this.payoutCard != -1 && card != -1) {
            return;
        }
        this.spriteCard.node.active = true;
        this.spriteCard.spriteFrame = ImageContainer.getInstance().framePokerBackCard;
        this.payoutCard = card;
        if(card == -1)
            return;
        this.playFlip(card);
    }

    async playFlip(card) {
        // if (!this.payoutCard)
        //     return;
        return new Promise<void>((resolve, reject) => {
            this.isPlayed = true;
            this.spriteCard.node.active = true;
            this.tweenCard = cc.tween(this.spriteCard.node);
            this.tweenCard
            .delay(0.2)
            .to(0.05, {scaleX : 0})
            .call(()=>{  this.spriteCard.spriteFrame = ImageContainer.getInstance().getPokerCard(card) })
            .to(0.05, {scaleX : 0.7})
            .call(() => {
                resolve();
            })
            .start();
        });

    }

    showFrameBackCard(){
        this.spriteCard.node.active = true;
        this.spriteCard.spriteFrame =  ImageContainer.getInstance().pokerCardBackFrame;
    }

    forceFlip() {
        this.tweenCard?.stop();
        this.spriteCard.node.active = true;
        this.spriteCard.node.scaleX = 0.6;
        this.spriteCard.spriteFrame = ImageContainer.getInstance().getCardFrameByCard(this.payoutCard);
    }

    reset() {
        this.payoutCard = null;
        this.tweenCard?.stop();
        this.spriteCard.node.active = false;
        this.isPlayed = false;
        this.isInit = false;
    }
}
