import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class RuleTable extends cc.Component {

    @property(cc.Button)
    closeBtn: cc.Button = null;

    @property(cc.Button)
    confirmBtn: cc.Button = null;

    start () {
        this.registerEvents();
    }

    registerEvents(){
        this.closeBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.hide, this);
        this.confirmBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.hide, this);
        
    }

    hide(){
        this.node.active = false;
        AudioPlayer.play(AudioID.Click2)
    }
}
