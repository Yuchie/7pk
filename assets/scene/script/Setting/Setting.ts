import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";
import { AUDIO_EVENT } from "../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Setting extends cc.Component {

    @property(cc.Button)
    closeBtn: cc.Button = null;

    @property(cc.Button)
    confirmBtn: cc.Button = null;

    @property(cc.Slider)
    musicSlider: cc.Slider = null;

    @property(cc.Slider)
    effectSlider: cc.Slider = null;

    private musicVolume: number = 0.5;
    private effectVolume: number = 0.5;
    private previouseMVolume: number = 0.5;
    private previousEVolume: number = 0.5;
    start () {
        this.registerEvents();
    }

    registerEvents(){
        this.closeBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.hide, this);
        this.confirmBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.confirm, this);
    }

    updateMusicVolume(event) {
        this.musicVolume = event.progress;
    }

    updateEffectVolume(event) {
        this.effectVolume = event.progress;
    }


    hide() {
        this.node.active = false;
        this.reset();
        AudioPlayer.play(AudioID.Click2)
    }

    confirm() {
        cc.systemEvent.emit(AUDIO_EVENT.UPDATE_MUSIC_VOLUME, this.musicVolume);
        cc.systemEvent.emit(AUDIO_EVENT.UPDATE_EFFECT_VOLUME, this.effectVolume);
        this.previouseMVolume = this.musicVolume;
        this.previousEVolume = this.effectVolume;
        this.node.active = false;
        AudioPlayer.play(AudioID.Click)
    }

    reset() {
        this.musicVolume = this.previouseMVolume;
        this.effectVolume = this.previousEVolume;
    }
    // update (dt) {}
}
