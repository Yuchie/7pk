import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SettingController extends cc.Component {
    @property(cc.Button)
    menuBtn: cc.Button = null;

    @property(cc.Node)
    menu: cc.Node = null;

    @property(cc.Button)
    settingBtn: cc.Button = null;

    @property(cc.Button)
    ruleBtn: cc.Button = null;

    @property(cc.Prefab)
    rulePrefab: cc.Prefab = null;

    @property(cc.Prefab)
    settingPrefab: cc.Prefab = null;

    private ruleTable = null;
    private settingTable = null;

    start () {
        this.registerEvent();
        this.menu.active = false;
    }

    registerEvent() {
        this.menuBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.show, this);
        this.ruleBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.createRuleTable, this);
        this.settingBtn.node.on(cc.Node.EventType.MOUSE_DOWN, this.createSettingTable, this);
    }

    createRuleTable() {
        if(this.ruleTable == null){
            this.ruleTable = cc.instantiate(this.rulePrefab);
            this.ruleTable.parent = cc.find("Canvas");
        }else{
           this.ruleTable.active= true;
        }
        this.hide();
        AudioPlayer.play(AudioID.Click)
    }

    createSettingTable() {
        if(this.settingTable == null){
            this.settingTable = cc.instantiate(this.settingPrefab);
            this.settingTable.parent = cc.find("Canvas");
        }else{
            this.settingTable.active = true;
        }
        this.hide();
        AudioPlayer.play(AudioID.Click)
    }

    hide() {
        this.menu.active = false;
    }

    show() {
        this.menu.active = !this.menu.active;
        AudioPlayer.play(AudioID.Click)
    }
    // update (dt) {}
}
