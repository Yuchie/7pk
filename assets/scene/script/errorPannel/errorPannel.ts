// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { NT } from "../../constant/constant";
import { NotifyComponent } from "../../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ErrorPannel extends NotifyComponent {


    @property(cc.Node)
    pannelNode: cc.Node = null;
    
    @property(cc.Label)
    showText : cc.Label = null ;

    start () {
        this.regist();
    }

    close(){
        this.pannelNode.active = false;

    }

    show(text: string ){
        this.showText.string = text;
        this.pannelNode.active=true;
        this.pannelNode.setPosition(964.496 , 979.714)
    }

    


    notify(data) {
        console.log("error", data);
       
        this.show(NT.ERROR_CODE[data.code]+"\n"+data.msg)
    
    }
        
    // update (dt) {}
}
