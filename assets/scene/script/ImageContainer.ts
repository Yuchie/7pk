import { NT } from "../constant/constant";
import HuluBet from "./Game/Bet/HuluBet";
import { GameSet } from "./Game/GameTypeSet";
import { DataManager } from "./Utils/DataManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ImageContainer extends cc.Component {

    static _instance = null;

    @property([cc.SpriteFrame])
    frameApple: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePacMan: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePokerClub: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePokerDiamond: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePokerSpade: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePokerHeart: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePaMan: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    frameSport: cc.SpriteFrame[] = [];

    @property(cc.SpriteFrame)
    frameAppleBackCard: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    framePokerBackCard: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    framePaManBackCard: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    framePacManBackCard: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    frameSportBackCard: cc.SpriteFrame = null;

    @property([cc.SpriteFrame])
    frameAppleJoker: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePacManJoker: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePaManJoker: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    frameSportJoker: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    framePokerJoker: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    frameRed: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    frameBlue: cc.SpriteFrame[] = [];

    @property([cc.SpriteFrame])
    frameBlack: cc.SpriteFrame[] = [];

    start(){
        cc.game.addPersistRootNode(this.node);
    }

    public getSpriteByVersion(spriteNum: any){
        switch(NT.version){
            case "poker":
               this.getPokerCard(spriteNum);
            break;
            case "sport":
                return this.frameSport[spriteNum];
            case "pacman":
                return this.framePacMan[spriteNum];
            case "paman":
                return this.framePaMan[spriteNum];
            case "apple":
                return this.frameApple[spriteNum];
        }
    }

    public getPokerCard(spriteNum:any){
        if(spriteNum == 52 || spriteNum == 53){
            return this.framePokerJoker[0];
        }else if(spriteNum == -1){
            return this.framePokerBackCard;
        }else{
            switch(Math.floor(spriteNum/13)){
                case GameSet.CardState.Spade:
                    //黑桃
                    var calcu = spriteNum%13;
                    console.log("黑桃", this.framePokerSpade[calcu])
                    return this.framePokerSpade[calcu];
                case GameSet.CardState.Hearts:
                    //紅心
                    var calcu = spriteNum%13;
                    console.log("紅心", this.framePokerHeart[calcu])
                    return this.framePokerHeart[calcu];
                case GameSet.CardState.Diamonds:
                    //方塊
                    var calcu = spriteNum%13;
                    console.log("方塊", this.framePokerDiamond[calcu])
                    return this.framePokerDiamond[calcu];
                case GameSet.CardState.Club:
                    //梅花
                    var calcu = spriteNum%13;
                    console.log("梅花", this.framePokerClub[calcu])
                    return this.framePokerClub[calcu];
            }
        }
    }

    public getGameNum(spriteNum: any){
        switch(NT.version){
            case "paman":
                return this.getColorNum(spriteNum);
            case "apple":
                var calcu = spriteNum%13;
                return this.frameBlack[calcu];
            case "pacman":
                var calcu = spriteNum%13;
                return this.frameBlack[calcu];
            case "sport":
                return this.getColorNum(spriteNum);
        }
    }
    
    public getColorNum(spriteNum: any){
        switch(Math.floor(spriteNum/13)){
            case GameSet.CardState.Spade:
                var calcu = spriteNum%13;
                return this.frameBlue[calcu];
            case GameSet.CardState.Diamonds:
                var calcu = spriteNum%13;
                return this.frameRed[calcu];
            case GameSet.CardState.Club:
                var calcu = spriteNum%13;
                return this.frameBlue[calcu];
            case GameSet.CardState.Hearts:
                var calcu = spriteNum%13;
                return this.frameRed[calcu];
        }
    }

    public getPaManFlower(spriteNum:any){
        if(spriteNum == 52){
            return this.framePaManJoker[0];
        }else if(spriteNum == 53){
            return this.framePaManJoker[1];
        }else if(spriteNum == -1){
            return this.framePaManBackCard;
        }else{
            return this.framePaMan[Math.floor(spriteNum/13)];
        }
    }

    public getPacManFlower(spriteNum: any) {
        if(spriteNum == 52){
            return this.framePacManJoker[0];
        }else if(spriteNum == 53){
            return this.framePacManJoker[1];
        }else if(spriteNum == -1){
            return this.framePacManBackCard;
        }else{
            return this.framePacMan[Math.floor(spriteNum/13)];   
        }
    }

    public getAppleFlower(spriteNum: any) {
        if(spriteNum == 52){
            return this.frameAppleJoker[0];
        }else if(spriteNum == 53){
            return this.frameAppleJoker[1];
        }else if(spriteNum == -1){
            return this.frameAppleBackCard;
        }else{
            console.log(this.frameApple[0], Math.floor(spriteNum/13));
            return this.frameApple[Math.floor(spriteNum/13)];  
        }
    }

    public getSportFlower(spriteNum: number) {
        if(spriteNum == 52){
            return this.frameSportJoker[0];
        }else if(spriteNum == 53){
            return this.frameSportJoker[1];
        }else if(spriteNum == -1){
            return this.frameSportBackCard;
        }else{
            return this.frameSport[Math.floor(spriteNum/13)];  
        }
    }



    static getInstance(){
        if(ImageContainer._instance == null){
            ImageContainer._instance = cc.find("ImageContainer").getComponent(ImageContainer); 
        }
        return ImageContainer._instance;
    }

}
