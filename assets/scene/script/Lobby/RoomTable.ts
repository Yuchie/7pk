import { core } from "../../Core/Core";
import { NotifyComponent } from "../../Core/view/NotifyComponent";
import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";
import { ANI_EVENTS } from "../Utils/Event";
import ClientToServer from "../proto/ClientToServer";
import Lobby from "./Lobby";

const {ccclass, property} = cc._decorator;

@ccclass
export default class RoomTable extends ClientToServer {
    
    @property(cc.Node)
    nodeRoom1: cc.Node = null;

    @property(cc.Node)
    nodeRoom2: cc.Node = null;

    @property(cc.Node)
    nodeRoom3: cc.Node = null;

    @property(cc.Sprite)
    spriteLight: cc.Sprite[] = [];

    @property(cc.Label)
    labelBetRange1: cc.Label = null;

    @property(cc.Label)
    labelBetRange2: cc.Label = null;

    @property(cc.Label)
    labelBetRange3: cc.Label = null;

    private roomData = null;
    private currentIndex = null;

    start () {
        this.reset();
    }

    reset(){
        this.resetLight();
        this.currentIndex = null;
        this.showRoomTable();
    }

    hideRoomTable(){
        this.node.active = false;
    }

    showRoomTable(){
        this.node.active = true;
    }

    resetLight(){
        this.spriteLight.forEach((sprite) => {
            sprite.node.active = false;
        })
    }

    setRoomBetRange(data: any){
        this.labelBetRange1.string = data.betranges[0].betName;
        this.labelBetRange2.string = data.betranges[1].betName;
        this.labelBetRange3.string = data.betranges[2].betName;
        this.roomData = data;
    }

    sendBetToServer(index: number){
        cc.systemEvent.emit(ANI_EVENTS.SHOW_LOADING_TWEEN);
        this.clientToServer("SetBet", 102, {betIndex: this.roomData.betranges[index].betIndex});
        // this.sendToServer(core.TYPE.PK5GAME, { 
        //     key: 'SetBet', 
        //     objKey: 102,
        //     data: {
        //         betIndex: this.roomData.betranges[index].betIndex
        //     }
        // });
        Lobby.miniBetMoney =  this.roomData.betranges[index].miniBetMoney; 
    }

    chooseRoom(event, customEventData){
        if(this.currentIndex == null){
            this.currentIndex = customEventData;
            this.resetLight();
            this.spriteLight[customEventData].node.active = true;
        }else{
            if(this.currentIndex == customEventData){
                this.sendBetToServer(customEventData);
                this.reset();
            }else{
                this.currentIndex = customEventData;
                this.resetLight();
                this.spriteLight[customEventData].node.active = true;
            }
        }
        AudioPlayer.play(AudioID.Click)
    }

}
