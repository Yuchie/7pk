import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";
import { ANI_EVENTS } from "../Utils/Event";
import prefabMachine from "../list/prefabMachine";
import ClientToServer from "../proto/ClientToServer";
import JpTable from "./JpTable";
import Lobby from "./Lobby";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MachineTable extends ClientToServer {

    @property([prefabMachine])
    machine: prefabMachine[] = [];

    @property(JpTable)
    jpTable: JpTable = null;

    @property(cc.Button)
    nextPageBtn: cc.Button = null;

    @property(cc.Button)
    previousPageBtn: cc.Button = null;

    @property(cc.Label)
    labelPage: cc.Label = null;

    @property(cc.Animation)
    animationPage: cc.Animation = null;

    private machineIds = [];
    private page: number = 1;
    public totalPage: number = 0;
    private leastMachineNum: number = 0;

    start() {
        this.init();
    }

    init() {
        let str = "0"+this.page;
        this.labelPage.string = str;
        this.nextPageBtn.node.on(cc.Node.EventType.TOUCH_END, this.nextPage.bind(this), this);
        this.previousPageBtn.node.on(cc.Node.EventType.TOUCH_END, this.previousPage.bind(this), this);
    }

    setMachineData(machineData: any){
        this.machineIds = this.reorderMachineID(machineData.machineIds);
        this.calculateTotalPageNum(this.machineIds);
        this.showMachineTable(this.machineIds);
    }

    calculateTotalPageNum(machines: any){
        this.totalPage = Math.ceil(machines.length/30);
        this.leastMachineNum = machines.length % 30;
    }

    reorderMachineID(machineIds: any){
        machineIds.sort(function(a, b) {
            return a - b;
        });
        return machineIds;
    }

    nextPage(){
        if(this.page >= this.totalPage)
        return;
        this.page++;
        if(this.page >= this.totalPage)
        this.machine.forEach(element => element.node.active = false);
        let str = "0"+this.page;
        this.labelPage.string = str;
        this.animationPage.play("nextPageAnimation");
        this.animationPage.on("finished", this.refreshTable.bind(this), this);
        AudioPlayer.play(AudioID.Click)
    }

    previousPage(){
        if(this.page <= 1)
        return;
        this.page--;
        let str = "0"+this.page;
        this.labelPage.string = str;
            this.animationPage.play("previousPageAnimation");
            this.animationPage.on("finished", this.refreshTable.bind(this), this);
            AudioPlayer.play(AudioID.Click)    
    }

    refreshTable(){
        if(this.page >= this.totalPage){
            let machineNum = (this.page -1) * 30 ;
            this.machine.forEach(element => element.node.active = false);
            for(let a = 0; a < this.leastMachineNum; a++){
                this.machine[a].node.active = true;
                this.machine[a].setMachineNum(this.machineIds[machineNum + a])
            }
        }else{
            this.machine.forEach(element => element.node.active = true);
            let machineNum = (this.page -1) * 30 ;
            this.machine.forEach((element, index) =>{
                this.machine[index].node.active = true;
                element.setMachineNum(this.machineIds[machineNum + index]);
            })
        }
    }
    
    showMachineTable(machineIds: any) {
        this.machine.forEach((element, index) => {
           element.setMachineNum(machineIds[index]);
           element.node.on("touchend",this.showJP.bind(this) , this);
        })
        this.node.active = true;
    }

    showJP(event){
        this.jpTable.setMachineId(event.target._children[1]._components[0].string);
        this.clientToServer("GetMachineDetial", 106, {machindId: event.target._children[1]._components[0].string});
        cc.systemEvent.emit(ANI_EVENTS.SHOW_LOADING_TWEEN);
        AudioPlayer.play(AudioID.Click)  
    }

    hideMachineTable(){
        this.node.active = false;
        AudioPlayer.play(AudioID.Click2)  
    }


    reset() {
        this.node.active = false;
        this.page = 0;
        this.machineIds = [];
    }
}
