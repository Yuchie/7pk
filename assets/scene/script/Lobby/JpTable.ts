import { NT } from "../../constant/constant";
import { AudioID } from "../Audio/AudioEnum";
import AudioPlayer from "../Audio/AudioPlayer";
import { BaseClass } from "../BaseClass";
import { LOBBY_EVENTS } from "../Utils/Event";
import ClientToServer from "../proto/ClientToServer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class JpTable extends ClientToServer {

    @property(cc.Label)
    labelJPList: cc.Label[] = [];

    private machineId = null;

    start(){
        this.node.active = false;
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(LOBBY_EVENTS.UPDATE_LOBBY_BONUS, (data) => {
            this.setProperty();
            this.showJpTable(data);
        })
    }

    setProperty() {
        if(!cc.isValid(this.node)){
            this.node = cc.find("Canvas/JpTable")
            let labelArr = [];
            cc.find("Canvas/JpTable").getComponentInChildren(cc.Layout).getComponentsInChildren(cc.Label).forEach((element, index) =>{
                labelArr.push(element);
            })
            this.labelJPList = labelArr;
        }
    }

    showJpTable(JpList: any){
        this.node.active = true;
        BaseClass.jpListResult(6);
        this.setJpString(JpList);
        // AudioPlayer.play(AudioID.Click)
    }

    setJpString(JpList: any){
        NT.jpList = JpList;
        this.labelJPList.forEach((jp,index) => {
            let jpStr = BaseClass.arrayJPList[index] +" "+ JpList[index];
            jp.string = jpStr;
        })
    }

    setMachineId(machineId: any){
        this.machineId = machineId;
    }

    isClicked: boolean = false;
    enterGame(){
        if(this.isClicked)
            return;
        this.isClicked = true;
        this.clientToServer("IntoGame", 108, {machindId: this.machineId});
        AudioPlayer.play(AudioID.Click)
    }

    hideJpTable(){
        this.node.active = false;
        AudioPlayer.play(AudioID.Click2)
    }
}
