import { NotifyComponent } from "../../Core/view/NotifyComponent";
import { core } from "../../Core/Core";
import { NT } from "../../constant/constant";
import GetMachineListRes from "../proto/GetMachineListRes";
import { BaseClass } from "../BaseClass";
import RoomTable from "./RoomTable";
import ClientToServer from "../proto/ClientToServer";
import MachineTable from "./MachineTable";
import JpTable from "./JpTable";
import { ANI_EVENTS, GAME_SCENE } from "../Utils/Event";
import AudioPlayer from "../Audio/AudioPlayer";
import { AudioID } from "../Audio/AudioEnum";
const {ccclass, property} = cc._decorator;

@ccclass
export default class Lobby extends ClientToServer {
 

    @property(cc.Component)
    RoomBtn : cc.Component[]= [];

    @property(cc.Button)
    backBtn: cc.Button = null;

    @property(RoomTable)
    roomTable:　RoomTable = null;

    @property(MachineTable)
    machineTable: MachineTable = null;

    private betRange = null;
    public static miniBetMoney = null;

     onLoad () {
        cc.director.preloadScene("Game");
        this.regist();
        NT.version = "poker";
        this.roomTable.showRoomTable();
        this.changeRoomStatus(true);
        AudioPlayer.playBGM(AudioID.Background);
     }

    start () {
        cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE);
        this.backBtn.node.on('click', this.backLobby, this);
        cc.systemEvent.emit(ANI_EVENTS.SHOW_LOADING_TWEEN);
        let time = setTimeout(()=>{
            this.clientToServer("GetBetRange", 100, {});}, 1000)
    }

    backLobby(){
        this.reset();
        this.changeRoomStatus(true);
        this.clientToServer("GetBetRange", 100, {});
        cc.systemEvent.emit(ANI_EVENTS.SHOW_LOADING_TWEEN);
        AudioPlayer.play(AudioID.Click)
    }

    //區間房狀態
    changeRoomStatus(status){
        for(var a = 0; a < this.RoomBtn.length; a++){
            this.RoomBtn[a].node.active = status;
        }
        this.backBtn.node.active = !status;
        // AudioPlayer.play(AudioID.Click)
    }

    Choose_Room(event, customEventData){
        this.clientToServer("SetBet", 102, {betIndex: this.betRange.betranges[customEventData].betIndex});
        AudioPlayer.play(AudioID.Click)
        Lobby.miniBetMoney = this.betRange.betranges[customEventData].miniBetMoney; 
    }

    reset() {
        this.roomTable.reset();
        this.machineTable.reset();
    }

    notify(data){
        this.betRange = data;
        this.roomTable.setRoomBetRange(data);
        cc.systemEvent.emit(ANI_EVENTS.HIDE_LOADING_TWEEN);
    }
 
}
