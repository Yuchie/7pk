
import {AudioID} from "./AudioEnum";
import AudioPlayer from "./AudioPlayer";

const {ccclass, property} = cc._decorator;

@ccclass('AudioButton')
export default class AudioButton extends cc.Component {

    @property(cc.Node)
    target: cc.Node = null;

    @property({type: cc.Enum(AudioID)})
    audioID: AudioID = AudioID.Background;

    start() {
        if(!this.target){
            this.target=this.node;
        }

        this.target.on(cc.Node.EventType.TOUCH_START,
            ()=>{
            AudioPlayer.play(this.audioID);
        },this);
    }
}