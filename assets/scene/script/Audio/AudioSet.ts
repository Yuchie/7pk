import {AudioID} from "./AudioEnum";
import AudioPlayer from "./AudioPlayer";

const {ccclass, property} = cc._decorator;

@ccclass('AudioSet')
export default class AudioSet {

    @property(cc.AudioClip)
    audioClip: cc.AudioClip = null;

    @property({type: cc.Enum(AudioID)})
    id: AudioID = AudioID.Bet;

    @property({type: cc.Float, range: [0, 1, 0.1], slide: true})
    volume: number = 1;

    @property(cc.Boolean)
    isBGM: boolean = false;

    @property(cc.Boolean)
    isLoop: boolean = false;

    @property({type: cc.Float, range: [0, 5, 0.1], slide: true})
    fadeInSec: number = 0;

    @property({type: cc.Float, range: [0, 5, 0.1], slide: true})
    fadeOutSec: number = 0;

    //==========================
    private runFade: boolean = false;
    private _fadeVol: number = 0
    private audioID: number = 0
    private playing: boolean = false;

    private audio: cc.AudioSource;
    private _volume: number = 1;

    public init() {
        if (this.isBGM) {
            this.audio = AudioPlayer.bgm;
        } else if (this.isLoop) {
            this.audio = AudioPlayer.loop;
        } else {
            this.audio = AudioPlayer.se;
        }
        this._volume=this.volume;
    }

    public set fadeVol(val: number) {
        this._fadeVol = val;
        cc.audioEngine.setVolume(this.audioID, this._fadeVol);
        this.audio.volume = this._fadeVol;
    }

    public get fadeVol(): number {
        return this._fadeVol;
    }

    private _fadeIn() {
        cc.tween(this)
            .delay(0.02)
            //.to(1, {fadeVol: this.bgmVol}, {easing:  EasingType.Fade})
            // @ts-ignore
            .to(this.fadeInSec, {fadeVol: this.volume})
            .start();

    }

    private _fadeOut() {
        cc.tween(this)
            .delay(0.02)
            // @ts-ignore
            .to(this.fadeOutSec, {fadeVol: 0})
            // .call(() => {this.onFadeOut();})
            .start();
    }


    private stopByID() {
        if (this.audioID != 0) {
            cc.audioEngine.stop(this.audioID);
            // this.audio.stop();
        }
    }

    //==========================
    public play() {
        if (this.isLoop) {
            this.fadeIn();
            this.playing = true;
            // this.asPlay(this.volume);
        } else {
            this.playOnce();
        }
    }

    public stop(forceStop: boolean = false) {
        if (this.isLoop) {
            if (forceStop) {
                this.stopByID();
                this.runFade = false;
                cc.Tween.stopAllByTarget(this);
            } else {
                this.fadeOut();
            }
            this.playing = false;
        } else {
            this.stopByID();
        }

    }

    public playOnce() {
        // cc.audioEngine.stop(this.audioID);
        // this.audioID = cc.audioEngine.play(this.audioClip, false, this.volume);
        // this.audio.playOneShot(this.audioClip, this.volume);
        cc.audioEngine.stop(this.audioID);
        this.audioID = cc.audioEngine.play(this.audioClip, false, this.volume);
    }

    private asPlay(volume: number) {
        this.audio.clip = this.audioClip;
        if(this.audio.volume!=0){
            this.audio.volume = volume;
        }
        this.audio.play();
    }

    public fadeIn() {
        if (this.runFade) {
            return;
        }

        if(this.fadeInSec>0){
            this.stopByID();
            this._fadeVol = 0;
            this.asPlay(this._fadeVol);
            this.runFade = true;
            this._fadeIn();
        }else{
            this.asPlay(this.volume);
        }


        this.playing = true;
    }

    public fadeOut() {
        if (this.runFade) {
            this.runFade = false;
            if(this.fadeOutSec>0) {
                this._fadeOut();
            }else{
                this.stopByID();
            }
        } else {
            this.stopByID();
        }

        this.playing = false;
    }

    public get isPlaying(): boolean {
        return this.playing;
    }

    public mute(m:boolean) {
        this.volume=m?0:this._volume;
        if(this.playing){
            this.audio.volume=this.volume;
        }
    }

}
