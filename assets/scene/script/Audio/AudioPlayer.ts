import AudioSet from "./AudioSet";
import {AudioID} from "./AudioEnum";
import { AUDIO_EVENT, EVENTS } from "../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass('AudioPlayer')
export default class AudioPlayer extends cc.Component {
    private bgmID: number = -1;

    @property(cc.AudioSource)
    seChannel: cc.AudioSource = null;

    @property(cc.AudioSource)
    loopChannel: cc.AudioSource = null;

    @property(cc.AudioSource)
    bgmChannel: cc.AudioSource = null;

    @property([AudioSet])
    audioList: AudioSet[] = [];
    //==========================
    private _enableSE: boolean = true;

    private _enableBGM: boolean = false;

    private idMap: object = {};
    //==========================
    public static instance: AudioPlayer;

    public static getInstance(): AudioPlayer {
        return AudioPlayer.instance;
    }
    //--------------------------
    public static get se(): cc.AudioSource{
        return AudioPlayer.instance.seChannel;
    }

    public static get loop(): cc.AudioSource{
        return AudioPlayer.instance.loopChannel;
    }

    public static get bgm(): cc.AudioSource{
        return AudioPlayer.instance.bgmChannel;
    }
    //==========================

    onLoad() {
        AudioPlayer.instance = this;
        this.initMap();
    }

    start() {
        this.registerEvents();
        this.updatMusicVolume(0.5);
        this.updateEffectVolume(0.5);
    }

    registerEvents() {
        cc.systemEvent.on(AUDIO_EVENT.UPDATE_MUSIC_VOLUME, (volume: number) => {
            this.updatMusicVolume(volume);
        })
        
        cc.systemEvent.on(AUDIO_EVENT.UPDATE_EFFECT_VOLUME, (volume: number) => {
            this.updateEffectVolume(volume);
        })
    }

    updatMusicVolume(volume: number){
        AudioPlayer.instance.bgmChannel.volume = volume;
    }

    updateEffectVolume(volume: number) {
        
        AudioPlayer.instance.seChannel.volume = volume;
        AudioPlayer.instance.loopChannel.volume = volume;
    }

    //==========================
    private initMap() {
        for (let audioSet: AudioSet, len = this.audioList.length, i: number = 0; i < len; i++) {
            audioSet = this.audioList[i];
            this.idMap[audioSet.id] = i;
            audioSet.init();
        }
    }

    private playSet(audioSet: AudioSet) {
        if (audioSet) {
            // Logger.d(`AudioPlayer.playSet:${AudioID[audioSet.id]}`)
            if (audioSet.isBGM && this._enableBGM) {
                audioSet.volume = AudioPlayer.instance.bgmChannel.volume;
                //重頭播放問題
                if(!audioSet.isPlaying) {
                    audioSet.play();
                }
                return;
            }
            audioSet.volume = AudioPlayer.instance.seChannel.volume;
            /*
            if (audioSet.isLoop){
                audioSet.play();
                return;
            }
            */

            if (this._enableSE) {
                audioSet.play();
            }

        }
    }


    private stopSet(audioSet: AudioSet, forceStop: boolean = false) {
        if (audioSet) {
            audioSet.stop(forceStop);
        }
    }
    //==========================
    public play(id: AudioID) {
        let index:number = 0;
        try {
            index = this.idMap[id];
        }catch (e) {
            return;
        }
        let audioSet: AudioSet = this.audioList[index];
        this.playSet(audioSet);
    }


    public stop(id: AudioID, forceStop: boolean = false) {
        let index:number = 0;
        try {
            // index = (typeof id == "number") ? this.idMap[id] : this.keyMap[id];
            index = this.idMap[id];
        }catch (e) {
            return;
        }
        let audioSet: AudioSet = this.audioList[index];
        this.stopSet(audioSet,forceStop);
    }

    
    public playBGM(id:AudioID = AudioID.Background) {
        if(id==null){
            if(this.bgmID==-1){
                return;
            }
        }else{
            if(this.bgmID != id && this.bgmID!=-1){
                this.stopBGM();
            }
            this.bgmID = id;
        }

        // this.play(this.bgmID,true);
        this.play(this.bgmID);

    }

    public stopBGM(fadeOut: boolean = true) {
        this.stop(this.bgmID, !fadeOut);
    }

    public delayPlay(id: AudioID,delayMS:number) {
        setTimeout(this.play.bind(this),delayMS,id);
    }
    //==========================
    public set enableSE(val: boolean) {
        this._enableSE = val;
        if (val) {
            this.loopChannel.volume=0.1;
        }else{
            this.seChannel.stop();
            this.loopChannel.volume=0;
        }
    }

    public set enableBGM(val: boolean) {
        if( this._enableBGM == val){
            return;
        }

        this._enableBGM = val;
        if (val) {
            this.playBGM();
        } else {
            this.stopBGM(false);
        }
    }

    public set enableAll(val: boolean) {
        this.enableSE=val;
        this.enableBGM=val;
    }

    //==========================

    public static play(id: AudioID) {
        //console.log("play id:" + id);
        if (this.instance) {
            console.log(this.instance);
            this.instance.play(id);
        }
        
    }

    public static stop(id: AudioID) {
        if (this.instance) {
            this.instance.stop(id);
        }
    }

    public static playBGM(id:AudioID) {
        if (this.instance) {
            this.instance.playBGM(id);
        }
       
    }

    public static stopBGM(fadeOut: boolean = true) {
        if (this.instance) {
            this.instance.stopBGM(fadeOut);
        }
    }

    public static delayPlay(id: AudioID,delayMS:number) {
        if (this.instance) {
            this.instance.delayPlay(id,delayMS);
        }
    }

    public static enableAll(val: boolean) {
        if (this.instance) {
            this.instance.enableAll=val;
        }
    }


}
