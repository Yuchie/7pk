import { NotifyComponent } from "../../Core/view/NotifyComponent";
import { EVENTS } from "../Utils/Event";
import { Utils } from "../Utils/Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends NotifyComponent {

    start () {

    }

    notify(result: any) {
        console.log("Connect Fail=====", result);
        cc.systemEvent.emit(EVENTS.SHOW_RECONNECT);
    }
}
