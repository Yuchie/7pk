import { core } from "../../Core/Core";
import { NotifyComponent } from "../../Core/view/NotifyComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ClientToServer extends NotifyComponent {

    clientToServer(key: string, objKey: number, data: any){
        this.sendToServer(core.TYPE.PK5Client, { 
            key: key, 
            objKey: objKey,
            data: data
        });
    }

}
