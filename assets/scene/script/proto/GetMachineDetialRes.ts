import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";
import JPBetList from "../Game/JP/JPBetList";
import JpTable from "../Lobby/JpTable";
import { ANI_EVENTS, GAME_EVENTS, JP_EVENT, LOBBY_EVENTS } from "../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GetMachineDetialRes extends NotifyComponent {

    onLoad() {
        this.regist(); 
    }
 
    notify(data) {
        console.log("GetMachineDetialRes", data);
        cc.systemEvent.emit(LOBBY_EVENTS.UPDATE_LOBBY_BONUS, data.jpList);
        cc.systemEvent.emit(ANI_EVENTS.HIDE_LOADING_TWEEN);
        JPBetList.bonusArray = data.jpList;
        // cc.systemEvent.emit(GAME_EVENTS.SAVE_BONUS_ARRAY, data.jpList);
    }
}
