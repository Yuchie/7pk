import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";
import AnimationPannel from "../Animation/AnimationPannel";
import ButtonPannl from "../Game/Button/ButtonPannel";
import Game from "../Game/Game";
import { GameSet } from "../Game/GameTypeSet";
import MiniGamePannel from "../MiniGame/MiniGamePannel";
import { GAME_EVENTS, MINI_GAME_EVENTS } from "../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BetDoubleRes extends NotifyComponent {

    @property(MiniGamePannel)
    miniGamePannel: MiniGamePannel = null;

    @property(Game)
    gamePannel: Game = null;

    @property(ButtonPannl)
    buttonScript: ButtonPannl = null;

    @property(AnimationPannel)
    animationPannel: AnimationPannel = null;
 
    start () {
        this.regist();
    }

    
    notify(data) {
        console.log("BetDoubleRes", data);
        this.miniGamePannel.setFinalPokcerList(data.DoubleBetList);
        this.miniGamePannel.showCards(this.miniGamePannel.getFinalPockerList());
        this.gamePannel.setUserMoney(data.nowGold);

        if(data.winResult.winGold > 0){
        //    this.miniGamePannel.miniGameWin(data);
           this.buttonScript.setButtonByStatus(GameSet.GameStatus.StartMiniGame);
        //    cc.systemEvent.emit(GAME_EVENTS.UPDATE_NORMAL_WIN_INDEX, data.winResult.winType);
           this.animationPannel.setWinLabel(data.winResult.winGold, true);
           this.miniGamePannel.setWinGold(data.winResult.winGold, true);
           this.miniGamePannel.setWinData(data);
           cc.systemEvent.emit(MINI_GAME_EVENTS.PLAY_BLINK_MINI);
        }else{
            console.log("MINIGAMELOSE====");
            this.miniGamePannel.setWinGold(data.nowGold, false);
            // this.miniGamePannel.setWinData(data);
            cc.systemEvent.emit(MINI_GAME_EVENTS.MINIGAME_END, data, true);
        }
       // this.gamePannel.endMiniGame(data);
        //更新金幣, data.nowGold
        //this.miniGamePannel.showCards(data.)

    
    }
        
}
