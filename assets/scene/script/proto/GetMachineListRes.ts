import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";
import Lobby from "../Lobby/Lobby";
import MachineTable from "../Lobby/MachineTable";
import RoomTable from "../Lobby/RoomTable";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GetMachineListRes extends NotifyComponent {

    @property(MachineTable)
    machineTable: MachineTable = null;

    @property(RoomTable)
    roomTable: RoomTable = null;

    onLoad() {
        this.regist();
    }
 
    

    
    notify(data) {
        console.log("GetMachineListRes", data);
        this.roomTable.hideRoomTable();
        this.machineTable.setMachineData(data);
        this.node.getComponent(Lobby).changeRoomStatus(false);

    }
        
  

    // update (dt) {}
}
