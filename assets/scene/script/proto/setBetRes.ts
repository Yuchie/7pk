import {NT} from "../../constant/constant";
import {core} from "../../Core/Core";
import {NotifyComponent} from "../../Core/view/NotifyComponent";
import { ANI_EVENTS } from "../Utils/Event";
const {ccclass, property} = cc._decorator;

@ccclass
export default class SetBetRes extends NotifyComponent {
    
    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
     
           
        this.regist();
       
        
    }

    notify(data) {
        console.log("SetBetRes", data);
        if(data.result){
            cc.systemEvent.emit(ANI_EVENTS.HIDE_LOADING_TWEEN);
            this.sendToServer(core.TYPE.PK5GAME, { 
                key: 'GetMachineList', 
                objKey: 104,
                data: {
                }
            });
            
        }
    }
        
  

    // update (dt) {}
}
