import { LocalCacheKey} from "../Config/LocalCacheKey";
import {i18nMgr} from "./i18n/i18nMgr";

export default class LocalCache {
    static instance = null;

    static getInstance() {
        if (!this.instance) {
            this.instance = new LocalCache()
        }
        return this.instance as LocalCache
    }

    /* *清除所有本地缓存数据 */
    removeAll() {
        cc.sys.localStorage.clear();
    }

    /**
     * 普通类型的存储方法
     *
     * @private
     * @param {string} key
     * @param {*} value
     */
    private put(key: string, value: any) {
        cc.sys.localStorage.setItem(key, value)
    }
    /**
     *
     *string的获取方法
     * @private
     * @template T              泛型只包含string,number,booblean
     * @param {string} key      
     * @param {T} [defaultValue]  如果没有值得时候返回的默认值
     */
    private getString(key: string, defaultValue?: string): string {
        let value: string = cc.sys.localStorage.getItem(key)
        let isHasdefut = defaultValue != undefined
        let isValueEmpty = value == undefined || value == null || value.length == 0
        if (isHasdefut && isValueEmpty) {
            return defaultValue;
        }
        return value;
    }
    /**
     *
     *string的获取方法
     * @private
     * @template T              泛型只包含string,number,booblean
     * @param {string} key      
     * @param {T} [defaultValue]  如果没有值得时候返回的默认值
     */
    private getNumber(key: string, defaultValue?: number): number {
        let value: string = cc.sys.localStorage.getItem(key)
        let isHasdefut = defaultValue != undefined
        let isValueEmpty = value == undefined || value == null || value.length == 0
        if (isHasdefut && isValueEmpty) {
            return defaultValue;
        }
        return Number(value);
    }
    /**
     *
     *booblean的获取方法
     * @private
     * @template T              泛型只包含string,number,booblean
     * @param {string} key      
     * @param {T} [defaultValue]  如果没有值得时候返回的默认值
     */
    private getBoolean(key: string, defaultValue?: boolean): boolean {
        let value: string = cc.sys.localStorage.getItem(key)
        let isHasdefut = defaultValue != undefined
        let isValueEmpty = value == undefined || value == null || value.length == 0
        if (isHasdefut && isValueEmpty) {
            return defaultValue;
        }
        return this.convertToBoolean(value);
    }
    private convertToBoolean(input: string): boolean {
        try {
            return JSON.parse(input);
        }
        catch (e) {
            return false;
        }
    }
    /**
     * JSon数据的存储
     *
     * @private
     * @param {string} key  
     * @param {any} value   
     */
    private putJson(key: string, value: any) {
        let jsonStr = JSON.stringify(value)
        cc.sys.localStorage.setItem(key, jsonStr)
    }
    /**
     *Json数据的获取 
     *
     * @private
     * @template T           根据泛型转成对应模型直接返回
     * @param {string} key       
     * @return {*}  {T}
     * @memberof AppLoaclCache
     */
    private getJson<T>(key: string): T {
        let jsonStr = cc.sys.localStorage.getItem(key)
        let value: T
        try {
            value = JSON.parse(jsonStr)
        } catch (error) {
            return null
        }
        return value;
    }

    setLanguage(lang){
        this.put(LocalCacheKey.LANGUAGE,lang);
    }

    getLanguage() {
        return this.getString(LocalCacheKey.LANGUAGE, i18nMgr.zh_cn);
    }

}