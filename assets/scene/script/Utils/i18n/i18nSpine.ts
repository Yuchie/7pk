import { i18nMgr } from "./i18nMgr";
import LocalCache from "../LocalCache";
const { ccclass, property, executeInEditMode, disallowMultiple, requireComponent, menu } = cc._decorator;

@ccclass
@executeInEditMode
@requireComponent(sp.Skeleton)
@disallowMultiple
@menu("i18n/i18nSpine")
export class i18nSpine extends cc.Component {

    @property({ visible: false })
    private i18n_string: string = "";

    @property({ visible: false })
    private i18n_zh_string: string = "";

    @property({ visible: false })
    private i18n_en_string: string = "";

    @property({ visible: false })
    private i18n_hk_string: string = "";

    @property({ visible: false })
    private i18n_vn_string: string = "";

    @property({ visible: false })
    private i18n_th_string: string = "";

    languageAnimationNameMap = {}

    @property({ type: cc.String, tooltip: CC_DEV && "spine文件名字" })
    get spineName() {
        return this.i18n_string;
    }

    set spineName(value: string) {
        this.i18n_string = value;
    }

    @property({ type: cc.String, tooltip: CC_DEV && "spine文件中文语言动画名称" })
    get zhAniStr() {
        return this.i18n_zh_string;
    }

    set zhAniStr(value: string) {
        this.i18n_zh_string = value
    }

    @property({ type: cc.String, tooltip: CC_DEV && "spine文件英文语言动画名称" })
    get enAniStr() {
        return this.i18n_en_string;
    }

    set enAniStr(value: string) {
        this.i18n_en_string = value
    }

    @property({ type: cc.String, tooltip: CC_DEV && "spine文件繁体语言动画名称" })
    get hkAniStr() {
        return this.i18n_hk_string;
    }

    set hkAniStr(value: string) {
        this.i18n_hk_string = value
    }

    @property({ type: cc.String, tooltip: CC_DEV && "spine文件越南语言动画名称" })
    get vnAniStr() {
        return this.i18n_vn_string;
    }

    set vnAniStr(value: string) {
        this.i18n_vn_string = value
    }


    @property({ type: cc.String, tooltip: CC_DEV && "spine文件泰国语言动画名称" })
    get thAniStr() {
        return this.i18n_th_string;
    }

    set thAniStr(value: string) {
        this.i18n_th_string = value
    }


    start() {
        i18nMgr._addOrDelSpkeleton(this, true);
        this._resetValue();
    }

    /**
     * 开始播放多语言动画
     * @param {Function} completeCallBack 完成回调
     * @param {string} defaultLanguageAni 默认语言动画
     * @param {boolean} loop 是否循环
     * @param {number} timeScale 播放速率
     * @memberof i18nSpine
     */
    playAnimation(completeCallBack: Function, loop: boolean, timeScale?: number, defaultLanguageAni?: string) {

        let skeleton = this.getComponent(sp.Skeleton);

        this.languageAnimationNameMap["zh"] = this.i18n_zh_string
        this.languageAnimationNameMap["en"] = this.i18n_en_string
        this.languageAnimationNameMap["hk"] = this.i18n_hk_string
        this.languageAnimationNameMap["vn"] = this.i18n_vn_string
        this.languageAnimationNameMap["th"] = this.i18n_th_string

        i18nMgr._getSpine(this.i18n_string, (spineData: sp.SkeletonData) => {
            skeleton.skeletonData = spineData
            skeleton.setAnimationCacheMode(sp.Skeleton.AnimationCacheMode.REALTIME)
            skeleton.defaultAnimation = "animation"
            skeleton.clearTracks()

            let language = LocalCache.getInstance().getLanguage();

            skeleton.timeScale = timeScale ? timeScale : 1

            if (this.languageAnimationNameMap[language]) {
                skeleton.setAnimation(0, this.languageAnimationNameMap[language], loop);
            } else {
                if (defaultLanguageAni) {
                    skeleton.setAnimation(0, this.languageAnimationNameMap[defaultLanguageAni], loop);
                } else {
                    cc.error("无指定播放多语言动画")
                }
            }

            skeleton.setCompleteListener(completeCallBack)
        });
    }

    _resetValue() {
        this.spineName = this.i18n_string;
    }

    onDestroy() {
        i18nMgr._addOrDelSpkeleton(this, false);
    }
}
