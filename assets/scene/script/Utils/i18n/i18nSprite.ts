import { i18nMgr } from "./i18nMgr";
import ResManger from "../UI/ResManger";
const { ccclass, property, executeInEditMode, disallowMultiple, requireComponent, menu } = cc._decorator;

@ccclass
@executeInEditMode
@requireComponent(cc.Sprite)
@disallowMultiple
@menu("i18n/i18nSprite")
export class i18nSprite extends cc.Component {

    @property({ visible: false })
    private i18n_string: string = "";

    start() {
        i18nMgr._addOrDelSprite(this, true);
        this._resetValue();
    }

    @property({ type: cc.String })
    get string() {
        return this.i18n_string;
    }

    set string(value: string) {
        this.i18n_string = value;
        this.setString(value);
    }

    @property({
        tooltip:"node size 不隨圖片大小改變"
    })
    fixNodeSize:boolean = false;

    async setString(value: string) {
        if (!value) {
            return;
        }
        this.i18n_string = value;
        let sprite = this.getComponent(cc.Sprite);
        if (cc.isValid(sprite)) {
            let spriteFrame:cc.SpriteFrame
            let path = "i18n/" + i18nMgr.getLang() + "/" + "sprite" + "/" + value;
            console.log("======path", path);
            if(ResManger.getInstance().loadPaths[path])
            {
                spriteFrame =  ResManger.getInstance().loadPaths[path];
            }else{
                spriteFrame = await i18nMgr.getLanguageSprite(value);
            }
            console.log("=====sprite", spriteFrame);
            if (cc.isValid(sprite)) {
                sprite.spriteFrame = spriteFrame;
                if (spriteFrame && !this.fixNodeSize) {
                    sprite.node.setContentSize(spriteFrame.getRect().width, spriteFrame.getRect().height)
                }
            }
        }
    }

    _resetValue() {
        this.setString(this.i18n_string);
    }

    onDestroy() {
        i18nMgr._addOrDelSprite(this, false);
    }
}
