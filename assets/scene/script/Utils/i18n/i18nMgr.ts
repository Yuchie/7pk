import * as i18nLabel from "./i18nLabel";
import * as i18nSprite from "./i18nSprite";
import * as i18nSpine from "./i18nSpine";
import ResManger from "../UI/ResManger";
import LocalCache from "../LocalCache";

// @ts-ignore
const languageData = require('LanguageData');

export class i18nMgr {
     private static language = "";     // 当前语言
     private static labelArr: i18nLabel.i18nLabel[] = [];        // i18nLabel 列表
     private static labelData: { [key: string]: string } = {};   // 文字配置
     private static spriteArr: i18nSprite.i18nSprite[] = [];       // i18nSprite 列表
     private static i18nSpineArr: i18nSpine.i18nSpine[] = [];       // i18nSpine 列表

     public static readonly en_us:string="en-us";
     public static readonly zh_cn:string="zh-cn";
     public static readonly arAllLang = [i18nMgr.zh_cn,i18nMgr.en_us];

     public static init(language?:string,cb = null) {
        if(!language){
            language = LocalCache.getInstance().getLanguage();
        }else if (this.language === language) {
             return;
        }
        this.setLanguage(language,cb);
    }

    public static getLang(){
         return this.language;
    }


    /**
     * 设置语言
     */
    private static setLanguage(language: string,cb) {
        let self = this
        self.language = language;
        LocalCache.getInstance().setLanguage(language)
        self.reloadLabel(cb);
        self.reloadSprite();
        self.reloadSpine();
    }


    /**
     * 添加或移除 i18nLabel
     */
    public static _addOrDelLabel(label: i18nLabel.i18nLabel,isAdd:boolean) {
        let index = this.labelArr.findIndex(l => l.node?.uuid === label.node?.uuid);
        if(isAdd)
        {
            if (index === -1) {
                this.labelArr.push(label);
            }else{
                this.labelArr[index] = label;
            }
        }else{
            if(index !== -1)
            {
                this.labelArr.splice(index, 1);
            }
        }
    }

    /**
     * 獲取多语言文字
     */
    public static _getLabel(key: string, params: string[]): string {
        this.init(this.language);
        let t = languageData.t(key);
        return cc.js.formatStr(t, ...params) || key
    }


    /**
     * 添加或移除 i18nSprite
     */
    public static _addOrDelSprite(sprite: i18nSprite.i18nSprite, isAdd: boolean) {
        let index = this.spriteArr.findIndex(s => s.node?.uuid === sprite.node?.uuid);
        if (isAdd) {
            if(index === -1)
            {
                this.spriteArr.push(sprite);
            }else{
                this.spriteArr[index] = sprite;
            }

        } else {
            if (index !== -1) {
                this.spriteArr.splice(index, 1);
            }
        }
    }

    /**
     * 預先獲取指定資料夾中所有語言的圖片
     * **/
    public static async preloadFolderAllLangSprite(folderPath) {
        for (const lang of this.arAllLang) {
            await this.loadFolderAllSprite(lang,folderPath);
        }
    }

    /**
     * 獲取資料夾中所有的圖片
     * **/
    public static async loadFolderAllSprite(lang,folderPath){
        let resPath = 'i18n/'+lang+'/sprite/'+folderPath;
        let assetsSource: Array<cc.Asset> = await ResManger.getInstance().loadDirRes(resPath)
        for (const asset of assetsSource) {
            if (asset instanceof cc.SpriteFrame) {
                  await ResManger.getInstance().loadLocalImag(resPath+'/'+asset.name);
            }
        }
    }


    /**
     * 獲取多语言圖片
     */
    public static async getLanguageSprite(key:string)
    {
        let path = "i18n/" + this.language + "/" + "sprite" + "/" + key;
        if(ResManger.getInstance().loadPaths[path])
        {
            return ResManger.getInstance().loadPaths[path];
        }
        return await ResManger.getInstance().loadLocalImag(path)
    }
    /**
    * 添加或移除 多语言 i18n Spine 
    */
    public static _addOrDelSpkeleton(spine: i18nSpine.i18nSpine, isAdd: boolean) {
        let index = this.i18nSpineArr.findIndex(s => s.node?.uuid === spine.node?.uuid);
        if (isAdd) {
            if(index === -1)
            {
                this.i18nSpineArr.push(spine);
            }else{
                this.i18nSpineArr[index] = spine;
            }

        } else {
            if (index !== -1) {
                this.i18nSpineArr.splice(index, 1);
            }
        }
    }

    public static async _getSpine(path: string, cb: (spine: sp.SkeletonData) => void) {
       // this.init(this.language);
        let assetsSource: Array<cc.Asset> = await ResManger.getInstance().loadDirRes(path)
        assetsSource.forEach(asset => {
            if (asset instanceof sp.SkeletonData) {
                cb(asset);
            }
        })
    }

    private static reloadLabel(cb) {
        let self = this;
        let url = "i18n/" + this.language + "/label/lang";
        ResManger.getInstance().loadRes(url, cc.JsonAsset, null, (err, data: cc.JsonAsset) => {
            if (err) {
                console.error(err);
                self.labelData = {};
            } else {
                Object.keys(data.json).forEach((key) => {
                    self.labelData[key] = data.json[key];
                })
                languageData.init(self.labelData)
            }
            for (let one of this.labelArr) {
                one._resetValue();
            }
            if(cb)
            {
                cb();
            }
        });
    }

    private static reloadSprite() {
        for (let one of this.spriteArr) {
            one._resetValue();
        }
    }

    private static reloadSpine() {
        for (let one of this.i18nSpineArr) {
            one._resetValue();
        }
    }

    public static getLabelJsonData() {
        return this.labelData
    }

    /**
     * 需要多语言中的拼接字符串
     * @param key 多语言中的key
     * @param params 使用的参数
     */
    public static getLanguageString(key, ...params) {
        let t = languageData.t(key);
        return cc.js.formatStr(t, ...params) || key
    }



    public static hasLanguageString(key){
        return languageData.has(key)
    }
    /**
     * 註冊label,當語言轉換時自動更新翻譯
     * @param label 顯示翻譯的label
     * @param key 多语言中的key
     * @param params 使用的参数
     */
    public static registeri18nLabel(label:cc.Label,key,...params){
        if(label)
        {
            let l
            if(label.node.getComponent("i18nLabel") === null)
            {
                l = label.node.addComponent("i18nLabel");
            }else{
                l = label.node.getComponent("i18nLabel");
            }
            l.string = key;
            l.params = params;
            this._addOrDelLabel(l,true)
        }
    }

    public static addDynamicTranslates(key,objWords){
        this.labelData[key] = objWords;
        languageData.init(this.labelData)
    }
}
