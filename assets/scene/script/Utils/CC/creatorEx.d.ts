declare namespace cc {
    export interface Node {
        _worldMatrix: cc.mat4;

        /* #region <Custom 自定義> */
        setAsFirstSibling(): void;
        setAsLastSibling(): void;
        /* #endregion <Custom 自定義> */
    }

    export interface Component {
        _vertsDirty: boolean;
        _materials: cc.Material[];

        _activateMaterial();
    }

    export interface Sprite {
        m_segLen: number;
        m_pointMass: number;
        _assembler: MassSpringAssembler;

        setVertsDirty(): void;
        _updateColor(): void;

        /* #region <Custom 自定義> */
        preserveSize(size: cc.Vec2): void;
        setNativeSize(): void;
        /* #endregion <Custom 自定義> */
    }

    export interface SpriteFrame {
        _uuid: string;
    }

    export interface Animation {
        /* #region <Custom 自定義> */
        playClips(index: number):cc.AnimationState;
        /* #endregion <Custom 自定義> */
    }

    export interface Graphics {
        _impl: cc.Graphics;
        _lineWidth: number;
        _lineJoin: cc.Graphics.LineJoin,
        _lineCap: cc.Graphics.LineCap,
        _strokeColor: cc.Color;
        _fillColor: cc.Color;
        _miterLimit: number;
    }

    export interface Color {
        _val: Uint32Array;
    }

    export class Assembler {
        init(comp: cc.Component);
    }

    export class RenderData {
        meshCount: number;
        iDatas: number[][];
        vDatas: Float32Array[];
        uintVDatas: Uint32Array[][];

        init(renderComp: cc.Assembler);
        createQuadData(index: number, verticesFloats: number, indicesCount: number);
        createData(index: number, verticesFloats: number, indicesCount: number);
    }

    export interface View {
        _frameSize: cc.Size;
        _orientation: number;
        _isRotated: boolean;
        _orientationChanging: boolean;

        _initFrameSize(): void;
        _resizeEvent(forceOrEvent: boolean);
    }

    export interface Label{
        _forceUpdateRenderData(bool:boolean);
    }
}

declare namespace cc.gfx {
    export function gfx(): void;

    const RB_FMT_S8: number;
}

declare namespace cc.MaterialVariant {
    export function gfx(): void;
}

declare namespace cc.screen {
    export function autoFullScreen(): void;
    export function exitFullScreen(): void;
    export function fullScreen(): boolean;
}

declare namespace cc.geomUtils.intersect {
    export function raycast(rootNode: cc.Node, ray: cc.geomUtils.Ray, handler?: Function, filter?: Function): any[];
}