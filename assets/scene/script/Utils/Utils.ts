export class Utils {
       // 計算放大位數
       public static getMin(nums: any): number {
        return Math.min(...nums);
    }

    /** 
     * 數字 轉換成 金額表示
     */
    public static toCurrency( num ) {
        let parts = num.toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return parts.join('.');
    }

    /** 
     * 轉換成 數字
     */
    public static toNumber( data ) {
        let num = data.replace(/[^\d\\.-]/g,'');
        return Number(num);
    }

    /** 
     * 取隨機數
     */
    public static getRandomNum(min:number , max:number):number{
        return  Math.floor(Math.random() * max) + min
    }

    public static async wait(time, callback?) {
        return new Promise((res, rej) => {
            setTimeout(() => {
                if (callback) {
                    callback()
                }
                res(null)
            }, time * 1000)
        })
    }


}