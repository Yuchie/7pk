import Object = cc.Object;

export default class ResManger {
    private static instance = null;
    private loadBundles = {};
    public static LOAD_UI_PROGRESS = "LOAD_UI_PROGRESS"
    public uiMaps = cc.js.createMap();
    public loadPaths = cc.js.createMap();
    public comResourcesPrefab = cc.js.createMap()
    static modules = {
        resources: "resources",
    };
    constructor() {
        this.loadBundles["resources"] = cc.resources
    }

    /**
     * 获取ResManager单例
     * @returns 
     */
    public static getInstance() {
        if (!this.instance) {
            this.instance = new ResManger();
        }
        return this.instance as ResManger
    }

    /**
     * 获取常用组件预制体
     * @param name 预制体名称
     * @returns 
     */
    public async getComResourcesPrefab(name: string) {
        let self = this
        if (self.comResourcesPrefab[name]) {
            return cc.instantiate(self.comResourcesPrefab[name])
        } else {
            return new Promise((res, rej) => {
                cc.resources.load("prefab/com/" + name, cc.Prefab, (err, asset) => {
                    self.comResourcesPrefab[name] = asset
                    res(cc.instantiate(asset))
                })
            })
        }
    }

    /**
     * 预加载UI
     * @param uiPrefabPath ui预制体路径
     * @param bundleName bundleName， 一般都是cc.resource
     * @param onProgress 加载进度回调
     * @param onComplete 完成回调
     * @returns 
     */
    public async preloadUI(uiPrefabPath: string, bundleName, onProgress?, onComplete?) {
        if (this.uiMaps[uiPrefabPath]) {
            if (onProgress) {
                onProgress(1, 1)
            }
            if (onComplete) {
                onComplete(null, this.uiMaps[uiPrefabPath])
            }
            return this.uiMaps[uiPrefabPath]
        }
        let bundle = await this.getBundle(bundleName) as cc.AssetManager.Bundle
        return await this.load(bundle, uiPrefabPath, cc.Prefab, onProgress, (err, assets) => {
            if (err) {
                return
            }
            this.uiMaps[uiPrefabPath] = assets
            if (onComplete) {
                onComplete(null, assets)
            }
        })
    }

     /**
     * 加载UI，并实例化
     * @param uiPrefabPath ui预制体路径
     * @param bundleName 一般底cc.resource
     * @returns 
     */
    public async loadUI(uiPrefabPath: string, bundleName?: string) {
        bundleName = bundleName || "resources"
        if (this.uiMaps[uiPrefabPath]) {
            return cc.instantiate(this.uiMaps[uiPrefabPath])
        }
        let assets = await this.preloadUI(uiPrefabPath, bundleName, null, null)
        return cc.instantiate(assets)
    }


    /**
     * 根据类型获取某个资源
     * @param path 路径
     * @param type 类型
     * @param onProgress 进度回调
     * @param onComplete 完成回调
     */
    public loadRes(path, type, onProgress?, onComplete?) {
        this.load(cc.resources, path, type, onProgress, onComplete)
    }

    /**
     * 加载图集资源
     * @param path 路径
     * @param onProgress 进度回调
     * @param onComplete 完成回调
     * @returns 
     */
    public async loadSpriteAtlasRes(path, onProgress?, onComplete?) {
        return await this.loadSpriteAtlas(cc.resources, path, onProgress, onComplete)
    }

    private async loadSpriteAtlas(bundle: cc.AssetManager.Bundle, path, onProgress?, onComplete?) {
        return new Promise<cc.SpriteAtlas>((res, rej) => {
            bundle.load(path, cc.SpriteAtlas, (p1, p2) => {
                if (onProgress) {
                    onProgress(p1, p2)
                }
            }, (err, assets: cc.SpriteAtlas) => {
                if (err) {
                    rej()
                    return
                }
                this.loadPaths[path] = assets
                if (onComplete) {
                    onComplete(null, assets)
                }
                res(assets)
            })
        })
    }

    /**
     * 加载某个文件夹中所有资源
     * @param path 路径
     * @param onProgress 进度回调
     * @param onComplete 完成回调
     * @returns 
     */
    public async loadDirRes(path, onProgress?, onComplete?){
        return await this.loadDir(cc.resources, path, onProgress, onComplete)
    }

    /**
     * 获取bundle
     * @param bundleName 名称
     * @returns 
     */
    public async getBundle(bundleName) {
        return new Promise((res, rej) => {
            let _bundle = this.loadBundles[bundleName]
            if (_bundle) {
                res(_bundle)
                return
            }
            cc.assetManager.loadBundle(bundleName, function (err: Error, bundle: cc.AssetManager.Bundle) {
                if (!err) {
                    res(bundle);
                    this.loadBundles[bundleName] = bundle
                } else {
                    rej(null);
                }
            }.bind(this))
        })
    }

    /**
     * 加载包（本地或者远程）
     * @param bundlePath 包路径
     * @param options 可选参数
     * @param onComplete 回调
     */
    public loadBundleWithCallBack(bundlePath: string, options: Record<string, any> , onComplete?){
        cc.assetManager.loadBundle(bundlePath, options, onComplete);
    }

    /**
     * 移除bundle
     * @param bundle 
     */
    public removeBundle(bundle: cc.AssetManager.Bundle){
        cc.assetManager.removeBundle(bundle)
    }

    /**
     * 加载远程图片资源
     * @param {cc.Sprite} containerSp 
     * @param {any} options 
     * @param {String} url 
     * @param {String} placeHolderImgStr 
     */
    public loadRemote(containerSp: cc.Sprite, options: any, url?: string, placeHolderImgStr?: string) {
        if (!url) {
            url = options
            cc.assetManager.loadRemote(url, function (err, texture: any) {
                if (err) {
                    if (placeHolderImgStr != null && placeHolderImgStr.length > 0) {
                        this.loadRes(placeHolderImgStr, cc.SpriteFrame, null, (error, asset) => {
                            if (cc.isValid(containerSp)) {
                                containerSp.spriteFrame = asset;
                            }
                        })
                    }
                    return
                }
                var sprite = new cc.SpriteFrame(texture);
                containerSp.spriteFrame = sprite;
            })
        } else {
            cc.assetManager.loadRemote(url, options, function (err, texture: any) {
                if (err) {
                    if (placeHolderImgStr != null && placeHolderImgStr.length > 0) {
                        this.loadRes(placeHolderImgStr, cc.SpriteFrame, null, (error, asset) => {
                            if (cc.isValid(containerSp)) {
                                containerSp.spriteFrame = asset;
                            }
                        })
                    }
                    return
                }
                var sprite = new cc.SpriteFrame(texture);
                containerSp.spriteFrame = sprite;
            })
        }
    }

    /**
     *加载本地图片资源
     *
     * @param {string} url
     * @param {string} errorPath 加载失败的时候，返回错误图片
     * @return {*} 
     * @memberof CommonBiz
     */
     public async loadLocalImagWithErrorPath(url: string, errorPath?: string) { 
        let resource = await this.loadLocalImag(url)
        if (resource) {
            return resource
        }
        if (errorPath) {
            let errorSource = await this.loadLocalImag(errorPath)
            return errorSource
        }
        return
    }

    /**
     * 加载远程图片资源
     * @param remoteUrl 远程路径
     * @param errorPath 加载失败的时候，返回错误图片
     * @param options 加载参数
     * @returns 
     */
    public async loadRemoteImagWithErrorPath(remoteUrl: string, errorPath?: string, options?:any) { 
        let resource = await this.loadRemoteImage(remoteUrl, options)
        if (resource) {
            return resource
        }
        if (errorPath) {
            let errorSource = await this.loadLocalImag(errorPath)
            return errorSource
        }
        return
    }

    /**
     * 加载本地图片
     * @param url 路径
     * @returns 
     */
    public async loadLocalImag(url: string) {
       return new Promise(resolve => {
            cc.resources.load(url,cc.SpriteFrame,(error: Error, spriteFrame: cc.SpriteFrame)=>{
                if (error) {
                    resolve(null)
                } else {
                    //resource.setPremultiplyAlpha(true);
                    //let spriteFrame:cc.SpriteFrame = new cc.SpriteFrame(resource);
                    if(spriteFrame)
                    {
                        //cc.dynamicAtlasManager.insertSpriteFrame(spriteFrame);
                        this.loadPaths[url] = spriteFrame
                      //  cc.log("cocos path:"+url);
                    }
                    resolve(spriteFrame)
                }
            })
        })
    }

    /**
     * 加载本地图片(回调)
     * @param url 路径
     * @returns 
     */
     public loadLocalImagWithCallBack(url: string, onComplete?) { 
        cc.resources.load(url, cc.SpriteFrame, (err, spriteFrame: cc.SpriteFrame) => {
            if (err) {
                onComplete(null);
                return
            }
            onComplete(spriteFrame);
        });
    }

    /**
     * 加载远程图片
     * @param url 远程路径
     * @param options 参数
     * @returns 
     */
    public async loadRemoteImage(url: string, options?: any){
        let self = this
        return new Promise<cc.SpriteFrame>(resolve => {
            if (options) {
                cc.assetManager.loadRemote(url, options, function (error: Error, resource: cc.Texture2D) {
                    if (error) {
                        resolve(null)
                    } else {
                        resource.setPremultiplyAlpha(true);
                        let spriteFrame = new cc.SpriteFrame(resource);
                        self.loadPaths[url] = spriteFrame
                        resolve(spriteFrame)
                    }
                })
            } else {
                cc.assetManager.loadRemote(url, function (error: Error, resource: cc.Texture2D) {
                    if (error) {
                        resolve(null)
                    } else {
                        resource.setPremultiplyAlpha(true);
                        let spriteFrame = new cc.SpriteFrame(resource);
                        self.loadPaths[url] = spriteFrame
                        resolve(spriteFrame)
                    }
                })
            }
        })
    }

    /**
     * 获取音效资源
     * @param path 资源路径
     * @returns {cc.AudioClip}
     */
    public async loadAudio(path: string) {
        return new Promise<cc.AudioClip>(resolve => {
            cc.resources.load(path, cc.AudioClip, null, function (err, clip: cc.AudioClip) {
                resolve(clip)
            });
        })
    }

    /**
     * 加载Json文件，resource下
     * @param url 
     * @returns 
     */
    public async loadJsonAsset(url: string) {
        return new Promise<any>(resolve => {
            cc.resources.load(url, function (error: Error, resource: cc.JsonAsset) {
                if (error) {
                    console.log("加载json文件失败:" + error);
                } else {
                    resolve(resource.json)
                }
            })
        })
    }

    /**
     * [private] 加载资源
     * @param bundle 包名称
     * @param path 路径
     * @param type 资源类型
     * @param onProgress 进度回调
     * @param onComplete 完成回调
     * @returns 
     */
    private async load(bundle: cc.AssetManager.Bundle, path, type, onProgress?, onComplete?) {
        return new Promise((res, rej) => {
            bundle.load(path, type, (p1, p2) => {
                if (onProgress) {
                    onProgress(p1, p2)
                }
            }, (err, assets) => {
                if (err) {
                    rej()
                    return
                }
                this.loadPaths[path] = assets
                if (onComplete) {
                    onComplete(null, assets)
                }
                res(assets)
            })
        }).catch(error =>{
            console.log("loadRes error = " + JSON.stringify(error) + " path = " + path)
        })
    }

    /**
     * [private] 加载目录所有资源
     * @param bundle 包名称
     * @param path 目录路径
     * @param onProgress 进度回调
     * @param onComplete 完成回调
     * @returns 
     */
    private async loadDir(bundle: cc.AssetManager.Bundle, path, onProgress?, onComplete?) {
        return new Promise<Array<cc.Asset>>((res, rej) => {
            bundle.loadDir(path, cc.Asset, (p1, p2) => {
                if (onProgress) {
                    onProgress(p1, p2)
                }
            }, (err, assets: cc.Asset[]) => {
                if (err) {
                    rej()
                    return
                }
                assets.forEach(asset => {
                    this.loadPaths[path] = asset
                });
                if (onComplete) {
                    onComplete(null, assets)
                }
                res(assets)
            })
        })
    }
}