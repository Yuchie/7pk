import HuluBet from "../Game/Bet/HuluBet";

export class DataManager {
    public static LangCode = 'en-us';
    public static huluBet = 0 ;
    public static pokerResult: any;
    public static locale = {
        "locale": "en-us",
    };

    public static get langCode() {
        return DataManager.locale.locale;
    }

        //取得得獎葫蘆數字
    public static getFHBonusWin(pokerList: any) {
        let pokerArr = pokerList.filter((element) =>{
            return element % 13;
        })

        let countObj = {}
        for(let x of pokerArr){
            countObj[x] = (countObj[x] || 0) + 1;
        }
        let vals = Object.values(countObj);
        if(DataManager.huluBet == vals[1] && (vals[0] === 2 && vals[1] === 3) || DataManager.huluBet == vals[0] && (vals[1] === 2 && vals[0] === 3)){
            return true;
        }
            return false;
        }

}