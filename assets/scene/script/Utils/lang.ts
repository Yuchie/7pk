export const GAME_TYPE_CN = {
    BAC: "百家乐",
    DTB: "龙虎",
    NIU: "牛牛",
    SIC: "骰宝",
    ROU: "轮盘",
};

export const GAME_TYPE_SPD_CN = "极速"
export const GAME_TYPE_SPD_EN = "SPD"
export const GAME_TYPE_BID_CN = "竞咪"
export const GAME_TYPE_BID_EN = "BID"

export const GAME_TYPE_EN = {
    BAC: 'BAC',
    DTB: 'DTB',
    NIU: 'NIU',
    SIC: 'SIC',
    ROU: 'ROU',
};

export const LIMIT_HEADER_CH = {
    'playType': '玩法',
    'tableLimit': '本台限红',
    'userLimit': '您的盘口',
    'mergedLimit': '投注区间',
    'outsideLimit': '无可用盘口',
}

export const LIMIT_HEADER_EN = {
    'playType': 'Game type',
    'tableLimit': 'Table limit',
    'userLimit': 'Your limit',
    'mergedLimit': 'Bet range',
    'outsideLimit': 'No valid limit',
}