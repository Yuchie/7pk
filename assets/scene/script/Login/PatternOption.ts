import { core } from "../../Core/Core";
import { NT } from "../../constant/constant";
const {ccclass, property} = cc._decorator;

@ccclass
export default class PatternOption extends cc.Component {

    @property(cc.Component)
    window: cc.Component = null;

    @property(cc.Component)
    square: cc.Component = null;

    @property(cc.Button)
    btnArr: cc.Button[] = [];

    start () {
        this.window.node.active = false;
    }

    showWindow(){
        if(this.window.node.active){
            this.window.node.active = false;
        }else{
            this.window.node.active = true;
            for(var a= 0 ; a < this.btnArr.length; a++){
                this.btnArr[a].node.on(cc.Node.EventType.TOUCH_START, this.chooseFunction, this);
                if(this.btnArr[a].node.name == NT.version){
                    this.square.node.position = this.btnArr[a].node.position;
                }
            }
            
            
        }
    }

    chooseFunction(eventTarget){
        this.square.node.active = true;
        this.square.node.position = eventTarget.target.position;
        NT.version = eventTarget.target.name;
    }

    closeWindow(){
        this.window.node.active = false;
    }

}
