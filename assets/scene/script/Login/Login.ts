import { NT } from "../../constant/constant";
import { core } from "../../Core/Core";
import { NotifyComponent } from "../../Core/view/NotifyComponent";
import { AuthConnect } from "../interface";
import User from "../User/User";
import { DataManager } from "../Utils/DataManager";
import { EVENTS } from "../Utils/Event";
import { i18nMgr } from "../Utils/i18n/i18nMgr";
import { Utils } from "../Utils/Utils";
const {ccclass, property} = cc._decorator;

@ccclass
export default class Login extends NotifyComponent {

    @property(cc.EditBox)
    account: cc.EditBox=null;

    @property(cc.EditBox)
    pwd : cc.EditBox=null;

    @property(cc.Node)
    connectWin: cc.Node = null;

    start () {
        cc.director.preloadScene("Load");
        this.registerEvents();
        this.connectToServer();
    }

    connectToServer() {
        this.regist();
        i18nMgr.init(DataManager.locale.locale);
        core.getInstance().connectTo(core.TYPE.PK5GAME , 1);

        this.account.string = "test";
        this.account.enabled = false;
        this.pwd.string = "test";
        this.pwd.enabled = false;
        this.connectWin.active = false;

        Utils.wait(2, () =>{
            this.autoLogin();
        })
    }
    
    //要圖
    showReconnectWindow() {
        this.connectWin.active = true;
        this.connectWin.getComponentInChildren(cc.Button).node.on(cc.Node.EventType.TOUCH_START, this.connectToServer.bind(this));
    }

    registerEvents() {
        this.connectWin.active = false;
        cc.systemEvent.on(EVENTS.SHOW_RECONNECT, ()=>{
            // this.connectToServer();
            this.showReconnectWindow();
        })
    }



    autoLogin(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'AuthConnect', 
            objKey: 2,
            data: {
                //  account: this.account.string,
                //  pwd: this.pwd.string,
                account: "test",
                pwd: "test",
                vendor: "10003"
            }
        });
    }

    sendInfo(){
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'AuthConnect', 
            objKey: 2,
            data: {
                 account: this.account.string,
                 pwd: this.pwd.string,
                // account: "test",
                // pwd: "test",
                vendor: "10003"
            }
        });
    }

    notify(data) {
        console.log(data);
        User.initData(data);
        cc.director.loadScene("Load");
    }
}
