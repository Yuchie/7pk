// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../Core/view/NotifyComponent";
import {core} from "../Core/Core";
import {NT} from "../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class enterGame extends NotifyComponent {

    onLoad () {
        this.regist();
    }

    
    notify(data) {
        if(data.result){
            cc.director.loadScene("Game");
        }
    }


    

  



 
}
