import BasePannel from "../Game/BasePannel";
import ImageContainer from "../ImageContainer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PacManMiniGamePannel extends BasePannel {

    showCards(cards: any) {
        cards.forEach((element, index) => {
            if(!this.spriteCards[index])
            return;
            this.spriteCards[index].node.active = true;
            this.spriteCards[index].spriteFrame = ImageContainer.getInstance().getPacManFlower(cards[index]);
            if(cards[index] == -1 || cards[index] == 52 || cards[index] == 53){
                this.hideNum(index);
            }else{
                this.showNum(index);
                this.spriteCards[index].getComponentInChildren(cc.Sprite).spriteFrame = ImageContainer.getInstance().getGameNum(cards[index]);
            }
        })
    } 

    playTitleAnimation(){
    }

    stopTitleAnimation(){
    }


   
}
