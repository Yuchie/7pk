import BasePannel from "../Game/BasePannel";
import ImageContainer from "../ImageContainer";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PokerMiniGamePannel extends BasePannel {

    start () {
    }

    showCards(cards: any) {
        cards.forEach((element, index) => {
            if(!this.spriteCards[index])
            return;
            this.spriteCards[index].node.active = true;
            this.spriteCards[index].spriteFrame = ImageContainer.getInstance().getPokerCard(cards[index]);
        })
    } 

    playTitleAnimation(){
    }

    stopTitleAnimation(){
    }

}
