const {ccclass, property} = cc._decorator;

@ccclass
export default class MiniGameRule extends cc.Component {
    
    @property(cc.Node)
    nodeMiniGameRule: cc.Node = null;

    start () {
        this.showMiniGameRule();
    }

    showMiniGameRule(){
        this.nodeMiniGameRule.active = true;
    }

    hideMiniGameRule(){
        this.nodeMiniGameRule.active = false;
    }

}
