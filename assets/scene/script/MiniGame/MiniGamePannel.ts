import { NT } from "../../constant/constant";
import AnimationPannel from "../Animation/AnimationPannel";
import JPAnimation from "../Animation/JPAnimation";
import { BaseClass } from "../BaseClass";
import Game from "../Game/Game";
import { GameSet } from "../Game/GameTypeSet";
import PaManPannel from "../Game/PaManPannel";
import PokerPannel from "../Game/PokerPannel";
import { ANI_EVENTS, GAME_EVENTS, MINI_GAME_EVENTS } from "../Utils/Event";
import AppleMiniGamePannel from "./AppleMiniGamePannel";
import PacManMiniGamePannel from "./PacManMiniGamePannel";
import PaManMiniGamePannel from "./PaManMiniGamePannel";
import PokerMiniGamePannel from "./PokerMiniGamePannel";
import SportMiniGamePannel from "./SportMiniGamePannel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MiniGamePannel extends cc.Component {

    @property(AppleMiniGamePannel)
    appleMiniGamePannel: AppleMiniGamePannel = null;

    @property(PacManMiniGamePannel)
    pacmanMiniGamePannel: PacManMiniGamePannel = null;

    @property(PaManMiniGamePannel)
    pamanMiniGamePannel: PaManMiniGamePannel = null;

    @property(SportMiniGamePannel)
    sportMiniGamePannel: SportMiniGamePannel = null;

    @property(PokerMiniGamePannel)
    pokerMiniGamePannel: PokerMiniGamePannel = null;

    @property(cc.Node)
    animationPannelNode: cc.Node = null;

    @property(JPAnimation)
    jpAnimation: JPAnimation = null;

    public winGold = 0;
    public pockers = [-1];
    public finalPockers = [];
    private winData: any ;

    start() {
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(MINI_GAME_EVENTS.SHOW_MINIGAME, this.showMiniGame, this);
        cc.systemEvent.on(MINI_GAME_EVENTS.MINIGAME_END, (winData, bool) => {
            let winType = BaseClass.jpListResult(this.winData.winResult.winType);
            console.log("BONUS____WIN_____BOOL", bool);
            if(!bool){
                this.miniGameEndAnimation(winData, bool);
                return;
            }
            if(winType == BaseClass.JP_NAME.STRAIGHT_FLUSH_WITH_A || winType == BaseClass.JP_NAME.FOUR_OF_KING_WITH_JOKER || winType == BaseClass.JP_NAME.STRAIGHT_FLUSH || winType == BaseClass.JP_NAME.FOUR_OF_KING){
                cc.systemEvent.emit(GAME_EVENTS.BONUS_WIN, winType);
                console.log("BONUS_______WIN", winType);
            }else if(winType == BaseClass.JP_NAME.FULL_HOUSE){
                if(Game.huluWin){
                    cc.systemEvent.emit(GAME_EVENTS.BONUS_WIN, winType);
                }else{
                    this.miniGameEndAnimation(winData, bool);
                }
            }
            else{
                this.miniGameEndAnimation(winData, bool);
            }
        })
    }

    setWinData(winData) {
        this.winData = winData;
    }

    setWinGold(winGold: number, isMiniGameWin: boolean){
        if(isMiniGameWin){
            this.winGold = this.winGold + winGold;
            console.log("setWinGold======== MINIGAME", this.winGold);
        }else{
            this.winGold = winGold;
        }
    }

    getWinGold(){
        return this.winGold;
    }

    setPockerList(pockers: any){
        this.pockers = [-1];
        for(var a = 0; a < 6; a++){
            this.pockers.push(pockers[a]);
        }
        console.log("=====setpockerlist", this.pockers);
    }

    miniGameWin(winData: any){
        this.animationPannelNode.getComponent(AnimationPannel).setWinLabel(winData.winResult.winGold, true);
        this.setPockerList(winData.DoubleBetList);
        this.showMiniGame();
        this.setWinGold(winData.winResult.winGold, true);
    }

    miniGameEndAnimation(winData: any, bonusEnd?:boolean){
        console.log("miniGameEndAnimation======", this.winData, AnimationPannel.normalWin);
        cc.systemEvent.emit(ANI_EVENTS.RUN_LABEL_TWEEN, bonusEnd);
    }

    endMiniGame(){
        return new Promise(resolve => {
            this.animationPannelNode.getComponent(AnimationPannel).resetJP();
            this.animationPannelNode.getComponent(AnimationPannel).reset();
            this.jpAnimation.reset();
            cc.find('Canvas').getComponent(Game).endMiniGame();
            cc.find('Canvas').getComponent(Game).updateUserMoneylabel();
            resolve;
        })
    }

    getPockerList(){
        return this.pockers;
    }

    setFinalPokcerList(pockers: any){
        this.finalPockers = [];
        this.finalPockers[0] = pockers[0];
        // let list = this.revertCard(pockers);
        for(var a = 1; a <= 6; a++){
            this.finalPockers.push(pockers[a]);
        }
    }

    getFinalPockerList(){
        return this.finalPockers;
    }

    revertCard(cards: any){
        let result = []
        for(let i = cards.length-1; i >= 0; i-- ){
            result.push(cards[i])
        }
            return result;
    }

    showMiniGame(){
        switch(NT.version){
            case "paman":
                this.pamanMiniGamePannel.reset();
                this.pamanMiniGamePannel.node.active = true;
                this.pamanMiniGamePannel.initStage();
                this.pamanMiniGamePannel.showCards(this.getPockerList());
                break;
            case "pacman":
                this.pacmanMiniGamePannel.reset();
                this.pacmanMiniGamePannel.node.active = true;
                this.pacmanMiniGamePannel.initStage();
                this.pacmanMiniGamePannel.showCards(this.getPockerList());
                break;
            case "apple":
                this.appleMiniGamePannel.reset();
                this.appleMiniGamePannel.node.active = true;
                this.appleMiniGamePannel.initStage();
                this.appleMiniGamePannel.showCards(this.getPockerList());
                break;
            case "sport":
                this.sportMiniGamePannel.reset();
                this.sportMiniGamePannel.node.active = true;
                this.sportMiniGamePannel.initStage();
                this.sportMiniGamePannel.showCards(this.getPockerList());
                break;
            case "poker":
                this.pokerMiniGamePannel.reset();
                this.pokerMiniGamePannel.node.active = true;
                this.pokerMiniGamePannel.initStage();
                this.pokerMiniGamePannel.showCards(this.getPockerList());
                break;
        }
    }

    showCards(pockers: any){
        switch(NT.version){
            case "paman":
                this.appleMiniGamePannel.showCards(pockers);
                break;
            case "pacman":
                this.pacmanMiniGamePannel.showCards(pockers);
                break;
            case "apple":
                this.appleMiniGamePannel.showCards(pockers);
                break;
            case "sport":
                this.sportMiniGamePannel.showCards(pockers);
                break;
            case "poker":
                this.pokerMiniGamePannel.showCards(pockers);
                break;
        }
    }


    reset() {
        this.winData = null;
        this.winGold = 0;
        this.pockers = [-1];
        this.finalPockers = [];
        this.appleMiniGamePannel.node.active = false;
        this.sportMiniGamePannel.node.active = false;
        this.pacmanMiniGamePannel.node.active = false;
        this.pamanMiniGamePannel.node.active = false;
        this.pokerMiniGamePannel.node.active = false;
        this.pokerMiniGamePannel.node.getComponent(PokerMiniGamePannel).reset();
        this.sportMiniGamePannel.getComponent(SportMiniGamePannel).reset();
        this.pacmanMiniGamePannel.getComponent(PacManMiniGamePannel).reset();
        this.pamanMiniGamePannel.getComponent(PaManMiniGamePannel).reset();
        this.appleMiniGamePannel.getComponent(AppleMiniGamePannel).reset();
    }


    
}
