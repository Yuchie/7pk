const {ccclass, property} = cc._decorator;


@ccclass
export default class BasePannel extends cc.Component {

    @property(cc.Sprite)
    spriteIcon: cc.Sprite = null;

    @property(cc.Animation)
    spriteTitle: cc.Animation = null;

    @property(cc.Sprite)
    spriteCards: cc.Sprite[] = [];

    @property
    isMiniGame : boolean = false;

    start(){
    }
    
    initStage(){
        if(this.isMiniGame){
            if(!this.spriteIcon)
            return;
            this.spriteIcon.node.active = true;
        }else{
            this.playTitleAnimation();
            if(!this.spriteIcon)
            return;
            this.spriteIcon.node.active = true;
        }
    }

    playTitleAnimation(){
        this.spriteTitle.node.active = true;
        this.spriteTitle.play("TitleAnimation");
    }

    stopTitleAnimation(){
        this.spriteTitle.node.active = false;
        this.spriteTitle.stop("TitleAnimation");
    }

    showCards(data: any){
    }

    showNum(index: number){
        this.spriteCards[index].getComponentInChildren(cc.Sprite).node.active = true;
    }

    hideNum(index: number){
        this.spriteCards[index].getComponentInChildren(cc.Sprite).node.active = false;
    }

    reset() {
        // if(this.spriteIcon.node.active){
        //     this.spriteIcon.node.active = false;
        // }
        this.spriteCards.forEach(element => {
            element.node.active = false;
        })
    }

}
