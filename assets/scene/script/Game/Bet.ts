

import { NT } from "../../constant/constant";
import { core } from "../../Core/Core";
import { NotifyComponent } from "../../Core/view/NotifyComponent";
import Lobby from "../Lobby/Lobby";
import BetGiveUpRes from "../proto/BetGiveUpRes";
import User from "../User/User";
import Game from "./Game";

const { ccclass, property } = cc._decorator;
@ccclass
export default class Bet extends NotifyComponent {
    
    @property(cc.Label)
    num: cc.Label[] = [];

    @property(cc.Label)
    point: cc.Label;

    private count : number = 0;
    public static bet_Num: number[] =[0,0,0,0] ;
    public static round: number = 0;
    public money = 0;
    private str = "POINT: ";
    onLoad() {
    }

    start () {
        this.point.string = this.str+User.getUserMoney();
        this.money = User.getUserMoney();

        for(var a =0 ; a < this.num.length; a++){
            this.num[a].string = "0";
            Bet.bet_Num[a] = 0;
        }
        this.count = 0;
    }

    reset(){
        for(var a =0 ; a < this.num.length; a++){
            this.num[a].string = "0";
            Bet.bet_Num[a] = 0;
        }
        this.count = 0;
    }

    allBet(){
        console.log("Lobby.miniBetMoney",Lobby.miniBetMoney ,Bet.bet_Num ,this.money ,Bet.round);
        if(Bet.bet_Num[Bet.round] <= 0 && this.money>=Bet.bet_Num[Bet.round] ){
            if(Bet.bet_Num[Bet.round] == 0){
                Bet.bet_Num[Bet.round] = Lobby.miniBetMoney[Lobby.miniBetMoney.length-1];
                this.num[Bet.round].string = Bet.bet_Num[Bet.round] + "";
            }else{
                Bet.round++;
            }
            cc.find("Canvas").getComponent(Game).sendBet();
            this.money = this.money- Bet.bet_Num[Bet.round];
            this.point.string = this.str+this.money;
        }
    }

    refreshGold(){
        this.money = User.getUserMoney();
        this.point.string = this.str+this.money;
    }

    betPlus(){
    //800 是葫蘆
    //1000 鐵支
    //2000 順子
    //4000 五支 (四張+鬼牌)
    //10000 同花大排
    //console.log(lobby.miniBetMoney);
        var count = Lobby.miniBetMoney[2] - Lobby.miniBetMoney[1];
        if(Bet.bet_Num[Bet.round] <= Lobby.miniBetMoney[Lobby.miniBetMoney.length-1]){
            if(Bet.bet_Num[Bet.round] == 0 && Bet.bet_Num[Bet.round] == null){
                Bet.bet_Num[Bet.round] = Lobby.miniBetMoney[Bet.round];
            }else{
                if(Bet.bet_Num[Bet.round] >= Bet.bet_Num[Bet.round-1]){
                    Bet.bet_Num[Bet.round] = Bet.bet_Num[Bet.round-1];
                }else{
                    if(Bet.bet_Num[Bet.round] >= Lobby.miniBetMoney[Lobby.miniBetMoney.length-1]){
                        Bet.bet_Num[Bet.round] = Lobby.miniBetMoney[Lobby.miniBetMoney.length-1];
                        
                    }else{
                        Bet.bet_Num[Bet.round] = Bet.bet_Num[Bet.round]+count;
                    }
                }  
            }
            this.num[Bet.round].string = Bet.bet_Num[Bet.round] + "";
            this.money = this.money - Bet.bet_Num[Bet.round];
            this.point.string = this.str+this.money; 
            NT.huluNum = NT.huluNum+ Bet.bet_Num[Bet.round];
            console.log("bet plus ===========", Bet.bet_Num[Bet.round], Lobby.miniBetMoney[Lobby.miniBetMoney.length-1]);
            if(Bet.bet_Num[Bet.round] == Lobby.miniBetMoney[Lobby.miniBetMoney.length-1]){
                cc.find("Canvas").getComponent(Game).sendBet();
            }
        }else{
            if(Bet.round < Lobby.miniBetMoney.length){
                Bet.round ++;
                this.betPlus();  
            }
           
        }
    }

}
