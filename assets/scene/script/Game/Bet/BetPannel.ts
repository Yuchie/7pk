import { NT } from "../../../constant/constant";
import Lobby from "../../Lobby/Lobby";
import { GAME_EVENTS } from "../../Utils/Event";
import Game from "../Game";

const { ccclass, property } = cc._decorator;
@ccclass
export default class BetPannel extends cc.Component {

    @property(cc.Label)
    labelBet: cc.Label[] = [];

    public betData: any;
    public betNum: number = 0;
    public betCurrentRound: number = 0;
    public betPreviousNum: number = 100;
    public betRange: number = 0;
    public betArray = [0,0,0,0];

    onLoad(){
        this.betData = Lobby.miniBetMoney;
        this.betRange = Lobby.miniBetMoney[2]-Lobby.miniBetMoney[1];
    }

    updateBetRound(){
        this.betPreviousNum = this.betNum;
        this.betNum = 0;
        this.betCurrentRound++;
    }

    updateLabelText(){
        if(this.betCurrentRound <= this.labelBet.length-1){
            this.labelBet[this.betCurrentRound].string = this.betNum+"";
        }
    }

    getCurrentBetNum(){
        return this.betNum;
    }

    betAll(){
        if(this.betCurrentRound > 0 ){
            this.betNum = this.betPreviousNum;
        }else{
            this.betNum = this.betData[this.betData.length-1];
        }
        this.updateLabelText();
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_BET_LIST, this.betNum);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_USER_POINT, this.betNum);
        cc.find("Canvas").getComponent(Game).sendBet();
    }

    bet(){
       this.betNum = this.betNum + this.betRange;
       if(this.betCurrentRound > 0){
            if(this.betNum >= this.betPreviousNum){
                this.betNum = this.betPreviousNum;
            }
       }else{
            if(this.betNum > this.betData[this.betData.length -1]){
                this.betNum = this.betData[this.betData.length -1];
            }
       }
       cc.systemEvent.emit(GAME_EVENTS.UPDATE_BET_LIST, this.betNum);
       cc.systemEvent.emit(GAME_EVENTS.UPDATE_USER_POINT, this.betNum);
       this.updateLabelText();
    }

    resetLabel(){
        this.labelBet.forEach(element => {
            element.string = "0";
        })
    }

    reset() {
        this.betNum = 0;
        this.betCurrentRound = 0;
        this.betPreviousNum = 100;
        this.betRange = Lobby.miniBetMoney[2]-Lobby.miniBetMoney[1];
        this.betArray = [0,0,0,0]
        this.labelBet.forEach(element => {
            element.string = "0";
        })
        cc.systemEvent.emit(GAME_EVENTS.RESET_BET_LIST);
    }


}
