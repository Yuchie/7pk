import { DataManager } from "../../Utils/DataManager";
import ButtonControl from "../Button/ButtonControl";

const { ccclass, property } = cc._decorator;
@ccclass
export default class HuluBet extends cc.Component{

    @property(cc.Label)
    labelhuluBet: cc.Label = null;

    public betCount = 1;

    start(){
        this.labelhuluBet.string = "F-"+this.betCount;
        this.updateHuluBet();
    }

    updateHuluBet() {
        DataManager.huluBet = this.betCount;
    }

    betMore(){
        this.betCount++;
        if(this.betCount >= 13){
            this.betCount = 13
        }
        this.labelhuluBet.string = "F-"+this.betCount;
        this.updateHuluBet();
    }

    betLess(){
        this.betCount--;
        if(this.betCount <= 1){
            this.betCount = 1;
        }
        this.labelhuluBet.string = "F-"+this.betCount;
        this.updateHuluBet();
    }

    reset() {
        DataManager.huluBet = 1;
        this.betCount = 1;
    }
    

}
