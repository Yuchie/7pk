import { BaseClass } from "../../BaseClass";
import { GAME_EVENTS } from "../../Utils/Event";
import Bet from "../Bet";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BetResult extends cc.Component {
    @property([cc.Label])
    nodeBetList: cc.Label[] = [];

    public betListNum = [];
  
    start () {
        console.log("sssssssssss1", this.nodeBetList);
    
        this.registerEvents();
        this.resetBetList();
        console.log("=========BetResult");
    }

    registerEvents() {
        cc.systemEvent.on(GAME_EVENTS.UPDATE_BET_LIST, (bet: number, round: number) => {
            this.updateBetList(bet, round);
        })

        cc.systemEvent.on(GAME_EVENTS.RESET_BET_LIST, (bet: number) => {
            this.resetBetList();
        })
    }
    
    updateBetList(bet: number, round: number) {
            if(this.nodeBetList == null){
                let fruits = cc.find("Canvas/WinFruit/WinLabels").children;
                let arr = [];
                fruits.forEach((element) =>{
                    arr.push(element.getComponentInChildren(cc.Label));
                })
                console.log("=========", arr);
                this.nodeBetList = arr;
                this.betListNum = [];
            }
        if(round == 0){
            this.nodeBetList.forEach((label, index) => {
                let calculateBet = BaseClass.getBetListByIndex(index) * bet * (round+1);
                label.string = calculateBet + "";
            })
            console.log("round == 0", round);
            this.betListNum[round] = bet;
        }else{
            this.nodeBetList.forEach((label, index) => {
                let cal = 0;
                for(let a = 0 ; a < round; a++) {
                    cal = cal + ((this.betListNum[a] * BaseClass.getBetListByIndex(index)));
                }
                // let calculateBet = BaseClass.getBetListByIndex(index) * bet * round + (this.betListNum[round -1] * BaseClass.getBetListByIndex(index));
                let calculateBet = BaseClass.getBetListByIndex(index) * bet + cal;
                label.string = calculateBet + "";
                // this.betListNum[index] = calculateBet;
            })
            this.betListNum[round] = bet;
        }
        
        console.log("Bet round", round)
    }

    resetBetList() {
        console.log("sssssssssss2", this.nodeBetList);
        this.betListNum = [];
        this.nodeBetList.forEach((label, index) => {
            label.string = BaseClass.getBetListByIndex(index)+"";
            // this.betListNum.push(BaseClass.getBetListByIndex(index));
        })
    }
    
}
