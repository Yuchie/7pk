// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { NT } from "../../constant/constant";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SmallGame extends cc.Component {

    @property(cc.Component)
    smallGameNode: cc.Component[] = [];

    private background = null;
    // LIFE-CYCLE CALLBACKS:
    private count = 0;

    private cardNum = [];
    private spriteArr = [];
    private piccount = 0;
  
    // onLoad () {}

    start () {
       // this.check_Game_version();
       // this.initPrefab();
        
    }

    check_Game_version(){
        switch(NT.version){
            case "poker":
                this.init_Poker();
            break;
            case "paman":
                this.init_PaMan();
            break;
            case "pacman":
                this.init_PacMan();
            break;
            case "sport":
                this.init_Sport();
            break;
            case "apple":
                this.init_Apple();
            break;
        }
    }


     //除了撲克牌以外的遊戲
     cards_for_otherGame(DoubleBetList){
        this.resetArray();

        for(var a = 0; a <this.smallGameNode.length; a++){
            
            this.cardNum.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(DoubleBetList[this.piccount]));
            this.spriteArr.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").calculateOtherGameFlower(DoubleBetList[this.piccount]))

            this.piccount++;
        }
        if(!this.smallGameNode[0].node.active){
            this.smallGameNode[0].node.active = true;
        }

        this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(NT.spritePath+NT.spriteName+"_05");
        this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum("-1");

        for(var a = 0; a < this.smallGameNode.length;a++){
            if(this.smallGameNode[a+1]){
            if(!this.smallGameNode[a+1].node.active){
                this.smallGameNode[a+1].node.active = true;
            }

                    if(DoubleBetList[a+1] == 52 || DoubleBetList[a+1] == "52" || DoubleBetList[a+1] == 53 || DoubleBetList[a+1] == "53"){
                        this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(this.spriteArr[a+1]);
                        this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum("ghost");
                    }else{
                        this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(this.spriteArr[a+1]);
                        this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum(this.cardNum[a+1]);
                
                    }
                    
            }
         }
        /*
        if(!this.smallGameNode[0].node.active){
            this.smallGameNode[0].node.active = true;
        }
        this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(NT.spritePath+NT.spriteName+"_05");
        this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum("-1");

         for(var a = 0; a < this.smallGameNode.length;a++){
            if(a+1 < this.smallGameNode.length){
                if(!this.smallGameNode[a+1].node.active){
                    this.smallGameNode[a+1].node.active = true;
                }
                this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(this.spriteArr[a]);
                    this.smallGameNode[a+1].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum(this.cardNum[a]);


            }
            

                    
                
         }

         */

    }
    resetArray(){
        this.spriteArr = [];
        this.piccount = 0;
        this.cardNum = [];
    }
    cards_for_poker(DoubleBetList){
        this.resetArray();
       
        for(var a = 0; a <this.smallGameNode.length; a++){
            
            this.spriteArr.push(this.node.getComponents("CardList")[0].node.getComponent("CardList").calculatePokerFlower(DoubleBetList[this.piccount]));

            this.piccount++;
        }
        if(!this.smallGameNode[0].node.active){
            this.smallGameNode[0].node.active = true;
        }
        this.smallGameNode[0].node.getChildByName("f1").getComponent("PrefabSprite").spritePic(NT.spritePath+NT.spriteName+"pokerback");
         for(var a = 0; a < this.smallGameNode.length;a++){
            if(a+1 < this.smallGameNode.length){
                if(!this.smallGameNode[a+1].node.active){
                    this.smallGameNode[a+1].node.active = true;
                }

                this.smallGameNode[a+1].node.getChildByName("f1").getComponent("PrefabSprite").spritePic(this.spriteArr[a]);
            }
                
         }
    }

    small_Game_Result_card(winPoker){

        switch(NT.version){
            case "poker":
                var flower = this.node.getComponents("CardList")[0].node.getComponent("CardList").calculatePokerFlower(winPoker[0]);
                this.smallGameNode[0].node.getChildByName("f1").getComponent("PrefabSprite").spritePic(flower);
            break;

            default:
                var num = this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(winPoker[0]);
                var flower = this.node.getComponents("CardList")[0].node.getComponent("CardList").calculateOtherGameFlower(winPoker[0]);

                this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spritePic(flower);
                this.smallGameNode[0].node.getChildByName(NT.spriteName+"_01").getComponent("PrefabSprite").spriteNum(num);

            break;
        }
      

        
    }

    smallGameOver(){
      var time = setTimeout(()=>{
        this.resetArray();
        for(var a= 0 ; a <　this.smallGameNode.length; a++){
            this.smallGameNode[a].node.active = false;
        }
        clearTimeout(time);
      },1500);
            
         
       
    }

     //動態建立prefab
    initPrefab(){
        switch(NT.version){
            case "poker":
                this.initPokerGamePrefab();
            break;

            default:
                this.initOtherGamePrefab();
            break;
        }
    }

    initOtherGamePrefab(){
          
        for(var a = 0 ;a < this.smallGameNode.length; a++){
            cc.resources.load(NT.path+NT.spriteName+"_01",  (err, prefab) => {
                this.background = cc.instantiate(prefab);
             //   this.background.getComponent("PrefabSprite").loadResource(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(data.pockers[a]));
                this.smallGameNode[this.count].node.addChild(this.background);
                
                this.smallGameNode[this.count].node.active = false;
                this.count ++;      
             });

        }
    }

    initPokerGamePrefab(){
        for(var a = 0 ;a < this.smallGameNode.length; a++){
            cc.resources.load(NT.path+"f1",  (err, prefab) => {
                this.background = cc.instantiate(prefab);
                
             //   this.background.getComponent("PrefabSprite").loadResource(this.node.getComponents("CardList")[0].node.getComponent("CardList").getCardNum(data.pockers[a]));
                this.smallGameNode[this.count].node.addChild(this.background);
                this.smallGameNode[this.count].node.active = false;
                this.count ++;      
            
             });


        }

    }

    init_PaMan(){
        var p = [
            [cc.v2(-668.915, -189.585)],
            [cc.v2(-10.822, -189.585)],
            [cc.v2(153.729, -189.585)],
            [cc.v2(319.857, -189.585)],
            [cc.v2(482.408, -189.585)],
            [cc.v2(649.208, -189.585)],
            [cc.v2(814.373, -189.585)]
        ]

        for(var a = 0; a < this.smallGameNode.length; a++){
            if(!this.smallGameNode[a].node.active){
                this.smallGameNode[a].node.active = true;
            }
            
            this.smallGameNode[a].node.scale = 0.7;
            this.smallGameNode[a].node.setPosition(p[a][0].x, p[a][0].y);
        }

    }

    init_PacMan(){
        var p = [
            [cc.v2(-668.915, -189.585)],
            [cc.v2(-10.822, -189.585)],
            [cc.v2(153.729, -189.585)],
            [cc.v2(319.857, -189.585)],
            [cc.v2(482.408, -189.585)],
            [cc.v2(649.208, -189.585)],
            [cc.v2(814.373, -189.585)]
        ]
        
        for(var a = 0; a < this.smallGameNode.length; a++){
            if(!this.smallGameNode[a].node.active){
                this.smallGameNode[a].node.active = true;
            }
            this.smallGameNode[a].node.scale = 1.2;
            this.smallGameNode[a].node.setPosition(p[a][0].x, p[a][0].y);
        }
    }

    init_Apple(){
        var p = [
            [cc.v2(-668.915, -189.585)],
            [cc.v2(-10.822, -189.585)],
            [cc.v2(153.729, -189.585)],
            [cc.v2(319.857, -189.585)],
            [cc.v2(482.408, -189.585)],
            [cc.v2(649.208, -189.585)],
            [cc.v2(814.373, -189.585)]
        ]
        

        for(var a = 0; a < this.smallGameNode.length; a++){
            if(!this.smallGameNode[a].node.active){
                this.smallGameNode[a].node.active = true;
            }
            this.smallGameNode[a].node.scale = 0.7;
            this.smallGameNode[a].node.setPosition(p[a][0].x, p[a][0].y);

        }



    }

    init_Poker(){
        var p = [
            [cc.v2(-668.915, -189.585)],
            [cc.v2(-10.822, -189.585)],
            [cc.v2(153.729, -189.585)],
            [cc.v2(319.857, -189.585)],
            [cc.v2(482.408, -189.585)],
            [cc.v2(649.208, -189.585)],
            [cc.v2(814.373, -189.585)]
        ]

        for(var a = 0; a < this.smallGameNode.length; a++){
            if(!this.smallGameNode[a].node.active){
                this.smallGameNode[a].node.active = true;
            }
            this.smallGameNode[a].node.scale = 0.8;
            this.smallGameNode[a].node.setPosition(p[a][0].x, p[a][0].y);
        }
    }
    init_Sport(){
        var p = [
            [cc.v2(-668.915, -189.585)],
            [cc.v2(-10.822, -189.585)],
            [cc.v2(153.729, -189.585)],
            [cc.v2(319.857, -189.585)],
            [cc.v2(482.408, -189.585)],
            [cc.v2(649.208, -189.585)],
            [cc.v2(814.373, -189.585)]
        ]

        for(var a = 0; a < this.smallGameNode.length; a++){
            if(!this.smallGameNode[a].node.active){
                this.smallGameNode[a].node.active = true;
            }
            this.smallGameNode[a].node.scale = 1.2;
            this.smallGameNode[a].node.setPosition(p[a][0].x, p[a][0].y);
        }

    }

    // update (dt) {}
}
