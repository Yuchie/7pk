import { NT } from "../../../constant/constant";
import { NotifyComponent } from "../../../Core/view/NotifyComponent";
import Lobby from "../../Lobby/Lobby";
import { GameSet } from "../GameTypeSet";

const { ccclass, property } = cc._decorator;
@ccclass
export default class ButtonControl extends NotifyComponent {

    //直接下注最大
    @property (cc.Button)
    betAllBtn: cc.Button = null;

    //單一 下注
    @property(cc.Button)
    betBtn: cc.Button = null;

    //小遊戲 選全押贏的錢
    @property(cc.Button)
    betBigMiniGameBtn: cc.Button = null;

    //小遊戲 選押贏的錢的一半
    @property(cc.Button)
    betSmallMiniGameBtn: cc.Button = null;

    //小遊戲 選押贏的錢的兩倍
    @property(cc.Button)
    betDoubleMiniGameBtn: cc.Button = null;

    //開牌
    @property(cc.Button)
    betOpenBtn: cc.Button = null;

    //返回大廳
    @property(cc.Button)
    backToLobbyBtn: cc.Button = null;

    @property(cc.Button)
    scroreBtn: cc.Button = null;

    onLoad() {
    
    }


}
