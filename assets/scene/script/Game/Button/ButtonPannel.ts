import ButtonControl from "./ButtonControl";
import Game from "../Game";
import HuluBet from "../Bet/HuluBet";
import { core } from "../../../Core/Core";
import MiniGamePannel from "../../MiniGame/MiniGamePannel";
import AnimationPannel from "../../Animation/AnimationPannel";
import { GameSet } from "../GameTypeSet";
import JPAnimation from "../../Animation/JPAnimation";
import BetLogic from "../Bet/BetLogic";
import { NT } from "../../../constant/constant";
import { GAME_EVENTS, MINI_GAME_EVENTS } from "../../Utils/Event";
import User from "../../User/User";
const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonPannl extends ButtonControl {
    @property(BetLogic)
    betPannel: BetLogic = null;

    @property(HuluBet)
    huluBet: HuluBet = null;

    @property(MiniGamePannel)
    miniGamePannel: MiniGamePannel = null;

    @property(cc.Node)
    animationPannelNode: cc.Node = null;

    @property(JPAnimation)
    jpAnimation: JPAnimation = null;

    public miniGameBetCount = 0;
    public betBigger = 0;
    public betSmaller = 0;
    private betMoney = 0 ;

    start() {
        // cc.game.addPersistRootNode(this.node);
        this.registerNode();
        this.setButtonByStatus(GameSet.GameStatus.UnStart);
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(GAME_EVENTS.UPDATE_GAME_STATUS, (status) =>{
            console.log("GAME_EVENTS.UPDATE_GAME_STATUS========", status);
            this.registerNode();
            this.setButtonByStatus(status);
        });

        cc.systemEvent.on(GAME_EVENTS.RESET_TARGET_OFF, () => {
            this.registerNode();
            this.targetOffAllFunction();
        })
    }

    registerNode() {
        if(this.betPannel == null) {
            this.betPannel = cc.find("Canvas/BetBg/BetTable/BetList").getComponent(BetLogic);
        }

        if(this.huluBet == null) {
            this.huluBet = cc.find("Canvas/HuluBG/HuluBet").getComponent(HuluBet);
        }

        if(this.miniGamePannel == null) {
            this.miniGamePannel = cc.find("Canvas/SmallGame").getComponent(MiniGamePannel);
        }

        if(this.animationPannelNode == null) {
            this.animationPannelNode = cc.find("Canvas/WinFruit");
        }

        if(this.jpAnimation == null) {
            this.jpAnimation = cc.find("Canvas/JPAnimation").getComponent(JPAnimation);
        }

        if(this.betAllBtn == null){
            this.betAllBtn = cc.find("Canvas/Buttons/GameBtn/BtnAllBet").getComponent(cc.Button);
        }
        if(this.betBtn == null){
            this.betBtn = cc.find("Canvas/Buttons/GameBtn/BtnBet").getComponent(cc.Button);
        }
        if(this.betBigMiniGameBtn == null){
            this.betBigMiniGameBtn = cc.find("Canvas/Buttons/MiniGameBtn/BtnBetBig").getComponent(cc.Button);
        }
        if(this.betSmallMiniGameBtn == null){
            this.betSmallMiniGameBtn = cc.find("Canvas/Buttons/MiniGameBtn/BtnBetSmall").getComponent(cc.Button);
        }
        if(this.betDoubleMiniGameBtn == null){
            this.betDoubleMiniGameBtn = cc.find("Canvas/Buttons/MiniGameBtn/BtnBetDouble").getComponent(cc.Button);
        }
        if(this.betOpenBtn == null){
            this.betOpenBtn = cc.find("Canvas/Buttons/GameBtn/BtnOpen").getComponent(cc.Button);
        }
        if(this.backToLobbyBtn == null){
            this.backToLobbyBtn = cc.find("Canvas/Buttons/BackToLobbyBtn").getComponent(cc.Button);
        }
        if(this.scroreBtn == null){
            this.scroreBtn = cc.find("Canvas/Buttons/ScoreBtn").getComponent(cc.Button);
        }
        
    }

    setButtonByStatus(status: number){
        switch(status){
            case GameSet.GameStatus.UnStart:
                this.showUnStartBtn();
                break;
            case GameSet.GameStatus.StartGame:
                this.showStartGameBtn();
                break;
            case GameSet.GameStatus.StartMiniGame:
                this.showStartMiniGameBtn();
                break;
            case GameSet.GameStatus.MiniGameChooseBet:
                this.showMiniGameChooseBetBtn();
                break;
            case GameSet.GameStatus.JPGame:
                this.skip();
                break;
            case GameSet.GameStatus.EndMiniGame:
                this.skip();
                break;
            case GameSet.GameStatus.EndGame:
                this.targetOffAllFunction();
                break;
            case GameSet.GameStatus.None:
                this.targetOffAllFunction();
                break;
        }
    }

    skip(){
        this.targetOffAllFunction();
       // this.scroreBtn.node.on(cc.Node.EventType.TOUCH_END, this.animationPannelNode.getComponent(AnimationPannel).forceJPLabelEnd, this.animationPannelNode.getComponent(AnimationPannel));
    }

    skipMiniGame(){
        this.betBigger = 0;
        this.betSmaller = 0;
        this.betMoney = this.miniGamePannel.getWinGold();
        cc.systemEvent.emit(GAME_EVENTS.START_MINI_GAME);
        let fakMoney = User.getUserMoney() - this.miniGamePannel.getWinGold();
        cc.find("Canvas").getComponent(Game).setFakerUserMoney(fakMoney);
        this.miniGamePannel.setWinGold(this.betMoney, false);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_WIN_LABEL, this.betMoney);
        cc.systemEvent.emit(MINI_GAME_EVENTS.STOP_BLINK_MINI);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.UnStart);
        cc.systemEvent.emit(MINI_GAME_EVENTS.MINIGAME_END, null, true);

    }

    showUnStartBtn(){
        this.targetOffAllFunction();
        this.betAllBtn.node.on(cc.Node.EventType.TOUCH_END, this.betPannel.betAll, this.betPannel);
        this.betBtn.node.on(cc.Node.EventType.TOUCH_END, this.betPannel.bet, this.betPannel);
        this.betOpenBtn.node.on(cc.Node.EventType.TOUCH_END, this.openCard, this);
        this.betBigMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.huluBetPlus, this);
        this.betSmallMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.huluBetMinus, this);
    }


    showStartGameBtn(){
        this.targetOffAllFunction();
        this.betAllBtn.node.on(cc.Node.EventType.TOUCH_END, this.betPannel.betAll, this.betPannel);
        this.betBtn.node.on(cc.Node.EventType.TOUCH_END, this.betPannel.bet, this.betPannel);
        this.betOpenBtn.node.on(cc.Node.EventType.TOUCH_END, this.openCard, this);
    }

    showStartMiniGameBtn(){
        this.targetOffAllFunction();
        this.betOpenBtn.node.on(cc.Node.EventType.TOUCH_END, this.skipMiniGame, this);
        this.betBigMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.betBig, this);
        this.betSmallMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.betSmall, this);
        this.betDoubleMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.betDouble, this);
    }

    showMiniGameChooseBetBtn(){
        this.targetOffAllFunction();
        this.betBigMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.chooseBiggerOrSmaller, this);
        this.betSmallMiniGameBtn.node.on(cc.Node.EventType.TOUCH_END, this.chooseBiggerOrSmaller, this);
    }


    targetOffAllFunction(){
        this.betAllBtn.node.targetOff(this);
        this.betBtn.node.targetOff(this);
        this.betAllBtn.node.targetOff(this.betPannel);
        this.betBtn.node.targetOff(this.betPannel);
        this.betOpenBtn.node.targetOff(this);
        this.betBigMiniGameBtn.node.targetOff(this);
        this.betSmallMiniGameBtn.node.targetOff( this);
        this.betDoubleMiniGameBtn.node.targetOff(this);
        this.scroreBtn.node.targetOff(this);
    }

    backToLobby() {
        cc.director.loadScene("Lobby");
    }

    chooseBiggerOrSmaller() {
        console.log("chooseBiggerOrSmaller======");
       this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'BetDouble', 
            objKey: 112,
            data: {
                betBigger: this.betBigger,
                betSmaller: this.betSmaller,
                betNum: this.betMoney
            }
        });
        this.targetOffAllFunction();
    }

    betBig(){
       this.betBigger = 1;
       this.betSmaller = 0;
       this.showMiniGameChooseBetBtn();
       this.betMoney = this.miniGamePannel.getWinGold()*2;
       cc.systemEvent.emit(GAME_EVENTS.START_MINI_GAME);
       let fakMoney = User.getUserMoney() - this.miniGamePannel.getWinGold();
       cc.find("Canvas").getComponent(Game).setFakerUserMoney(fakMoney);
       this.miniGamePannel.setWinGold(0, false);
    //    this.animationPannelNode.getComponent(AnimationPannel).setWinLabel(0);
       cc.systemEvent.emit(GAME_EVENTS.UPDATE_WIN_LABEL, 0);
       cc.systemEvent.emit(MINI_GAME_EVENTS.STOP_BLINK_MINI);
       
    }

    betSmall(){
        this.betBigger = 0;
        this.betSmaller = 1;
        this.showMiniGameChooseBetBtn();
        this.betMoney = this.miniGamePannel.getWinGold()/2;
        this.miniGamePannel.setWinGold(this.betMoney, false);
        console.log("betsmall=============", this.betMoney);
        cc.systemEvent.emit(GAME_EVENTS.START_MINI_GAME);
        // this.animationPannelNode.getComponent(AnimationPannel).setWinLabel(this.betMoney);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_WIN_LABEL, this.betMoney);
        cc.systemEvent.emit(MINI_GAME_EVENTS.STOP_BLINK_MINI);
    }

    betDouble(){
        this.betBigger = 1;
        this.betSmaller = 0;
        this.showMiniGameChooseBetBtn();
        this.betMoney = this.miniGamePannel.getWinGold();
        this.miniGamePannel.setWinGold(0, false);
        cc.systemEvent.emit(GAME_EVENTS.START_MINI_GAME);
        // this.animationPannelNode.getComponent(AnimationPannel).setWinLabel(0);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_WIN_LABEL, 0);
        cc.systemEvent.emit(MINI_GAME_EVENTS.STOP_BLINK_MINI);
    }

    huluBetPlus(){
        this.huluBet.betMore();
    }

    huluBetMinus(){
        this.huluBet.betLess();
    }

    reset() {
        this.miniGameBetCount = 0;
        this.betBigger = 0;
        this.betSmaller = 0;
    }

    openCard(){
        cc.find("Canvas").getComponent(Game).sendBet();
    }



}
