import BasePannel from "./BasePannel";
import ImageContainer from "../ImageContainer";
const {ccclass, property} = cc._decorator;

@ccclass
export default class ApplePannel extends BasePannel {


    showCards(cards: any) {
        cards.forEach((element, index) => {
            this.spriteCards[index].node.active = true;
            this.spriteCards[index].spriteFrame = ImageContainer.getInstance().getAppleFlower(cards[index]);
            if(cards[index] == -1 || cards[index] == 52 || cards[index] == 53){
                this.hideNum(index);
            }else{
                this.showNum(index);
                this.spriteCards[index].getComponentInChildren(cc.Sprite).spriteFrame = ImageContainer.getInstance().getGameNum(cards[index]);
            }
            
        })
    }
    
}
