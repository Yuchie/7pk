import { NT } from "../../constant/constant";
import { core } from "../../Core/Core";
import { NotifyComponent } from "../../Core/view/NotifyComponent";
import HuluBet from "./Bet/HuluBet";
import JPBetList from "./JP/JPBetList";
import PaManPannel from "./PaManPannel";
import PokerPannel from "./PokerPannel";
import SportPannel from "./SportPannel";
import ApplePannel from "./ApplePannel";
import PacManPannel from "./PacManPannel";
import { GameSet } from "./GameTypeSet";
import Lobby from "../Lobby/Lobby";
import MiniGamePannel from "../MiniGame/MiniGamePannel";
import MiniGameRule from "../MiniGame/MiniGameRule";
import AnimationPannel from "../Animation/AnimationPannel";
import JPAnimation from "../Animation/JPAnimation";
import BetLogic from "./Bet/BetLogic";
import ButtonPannl from "./Button/ButtonPannel";
import { ANI_EVENTS, GAME_EVENTS, MINI_GAME_EVENTS, WIN_TYPE } from "../Utils/Event";
import { DataManager } from "../Utils/DataManager";
import User from "../User/User";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Game extends NotifyComponent {
    
    @property(cc.Node)
    nodeSport: cc.Node = null;

    @property(cc.Node)
    nodePaMan: cc.Node = null;

    @property(cc.Node)
    nodePacMan: cc.Node = null;

    @property(cc.Node)
    nodeApple: cc.Node = null;

    @property(cc.Node)
    nodePoker: cc.Node = null;

    @property(BetLogic)
    betPannel: BetLogic = null;

    @property(MiniGamePannel)
    miniGame: MiniGamePannel = null;

    @property(MiniGameRule)
    miniGameRule: MiniGameRule = null;

    @property(cc.Node)
    animationPannel: cc.Node = null;

    @property(JPAnimation)
    jpAnimation: JPAnimation = null;

    @property(cc.Label)
    labelUserMoney: cc.Label = null;

    private tweenNode: cc.Tween;
    private tweenMiniNode: cc.Tween;
    public static huluWin: boolean;

    onLoad() {
        //this.reset();
        this.showGameByVersion();
    }

    start () {
        this.regist();
        this.tweenNode = cc.tween(this.node);
        this.tweenMiniNode = cc.tween(this.miniGame.node);
        this.updateUserMoneylabel();
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(GAME_EVENTS.UPDATE_USER_GOLD, (gold: any)=>{
            this.playUserGoldAni(gold);
        })

        cc.systemEvent.on(GAME_EVENTS.RESET_GAME, ()=> {
            this.reset();
        })

        cc.systemEvent.on(GAME_EVENTS.START_MINI_GAME, () => {
            this.startMiniGame();
        })

        cc.systemEvent.on(GAME_EVENTS.UPDATE_USER_POINT, (betNum: number) => {
            console.log("GAME_EVENTS.UPDATE_USER_POINT=======", betNum);
            this.updateUserMoneyAfterBet(betNum);
        })

    }

    playUserGoldAni(gold) {
        let currentMoney = User.getUserMoney() - gold;
        let userGold = {result: 0};
        cc.tween(userGold).to(1, {result: gold}, {
            progress: (start, end, current, ratio) => {
                let num = Math.floor(gold * ratio);
                console.log(start,ratio, num);
                let str = "POINT: ";
                this.labelUserMoney.string = str + (currentMoney + num);
                return num;
            }
        })
        .call(()=>{
            let str = "POINT: ";
            this.labelUserMoney.string = str + User.getUserMoney();
        })
        .start()
    }

    setFakerUserMoney(money: number){
        let str = "POINT: ";
        this.labelUserMoney.string = str + money;
    }

    setUserMoney(money: number){
        User.setUserMoney(money);
    }

    updateUserMoneyAfterBet(bet: number) {
        let str = "POINT: ";
        let money = User.getUserMoney() - bet;
        this.labelUserMoney.string = str+ money;
    }

    updateUserMoneylabel(){
        let str = "POINT: ";
        this.labelUserMoney.string = str+ User.getUserMoney();
    }

    showGameByVersion(){
        //this.reset();
        switch(NT.version){
            case "paman":
                this.nodePaMan.active = true;
                this.nodePaMan.getComponent(PaManPannel).initStage();
                break;
            case "pacman":
                this.nodePacMan.active = true;
                this.nodePacMan.getComponent(PacManPannel).initStage();
                break;
            case "apple":
                this.nodeApple.active = true;
                this.nodeApple.getComponent(ApplePannel).initStage();
                break;
            case "sport":
                this.nodeSport.active = true;
                this.nodeSport.getComponent(SportPannel).initStage();
                break;
            case "poker":
                this.nodePoker.active = true;
                this.nodePoker.getComponent(PokerPannel).initStage();
                break;
        }
    }

    showCards(pockers: any){
        switch(NT.version){
            case "paman":
                this.nodePaMan.getComponent(PaManPannel).showCards(pockers);
                break;
            case "pacman":
                this.nodePacMan.getComponent(PacManPannel).showCards(pockers);
                break;
            case "apple":
                this.nodeApple.getComponent(ApplePannel).showCards(pockers);
                break;
            case "sport":
                this.nodeSport.getComponent(SportPannel).showCards(pockers);
                break;
            case "poker":
                this.nodePoker.getComponent(PokerPannel).showCards(pockers);
                break;
        }
    }

    stopTitleAnimation(){
        switch(NT.version){
            case "paman":
                this.nodePaMan.getComponent(PaManPannel).stopTitleAnimation();
                break;
            case "pacman":
                this.nodePacMan.getComponent(PacManPannel).stopTitleAnimation();
                break;
            case "apple":
                this.nodeApple.getComponent(ApplePannel).stopTitleAnimation();
                break;
            case "sport":
                this.nodeSport.getComponent(SportPannel).stopTitleAnimation();
                break;
            case "poker":
                this.nodePoker.getComponent(PokerPannel).stopTitleAnimation();
                break;
        }
    }

    playTitleAnimation(){
        switch(NT.version){
            case "paman":
                this.nodePaMan.getComponent(PaManPannel).playTitleAnimation();
                break;
            case "pacman":
                this.nodePacMan.getComponent(PacManPannel).playTitleAnimation();
                break;
            case "apple":
                this.nodeApple.getComponent(ApplePannel).playTitleAnimation();
                break;
            case "sport":
                this.nodeSport.getComponent(SportPannel).playTitleAnimation();
                break;
            case "poker":
                this.nodePoker.getComponent(PokerPannel).playTitleAnimation();
                break;
        }
    }

    sendBet(){
        let currentBet = this.betPannel.getCurrentBetNum();
        if(currentBet < Lobby.miniBetMoney[0])
            return;
        this.sendToServer(core.TYPE.PK5GAME, { 
            key: 'Bet', 
            objKey: 110,
            data: {
                betNum: this.betPannel.getCurrentBetNum(),
                f: DataManager.huluBet,
            }
        });
        cc.systemEvent.emit(GAME_EVENTS.RESET_TARGET_OFF);
        cc.systemEvent.emit(ANI_EVENTS.SHOW_LOADING_TWEEN);
    }

    startGame(data: any){
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.StartGame);
        this.betPannel.node.active =  true;
       // this.miniGameRule.node.active = false;
        this.betPannel.updateBetRound();
        this.stopTitleAnimation();
    }

    winOrLose(winData: any, pockers: number, data: any){
        this.showCards(data.pockers);
        cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_BONUS, data.jpList);
        cc.systemEvent.emit(GAME_EVENTS.RESET_TARGET_OFF);

        //輸
        if(winData.winResult.winGold <= 0 && data.pockers.length >= 7){
            this.tweenNode?.stop();
            this.tweenMiniNode?.stop();
            this.tweenNode
            .delay(1)
            .call(() => {
                this.betPannel.reset();
                this.updateUserMoneylabel();
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.UnStart);
                this.reset();
                this.showGameByVersion();
            })
            .start()
        }else{
            this.startGame(data);  
        }
        //贏
        let winjp2 = 10000;
        if(winData.winResult.winGold > 0 && winData.winResult.winPoker.length > 0){
            //jp win
            if(winData.winResult.winJP2Gold > 0){
                console.log("jp win====");
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.None);
                this.miniGame.setPockerList(winData.DoubleBetList);
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_NORMAL_WIN_INDEX, winData.winResult.winType);
                Game.huluWin = this.checkHuluWin(winData);
                this.miniGame.setWinGold(winData.winResult.winGold, false);
                this.miniGame.setWinData(winData);
                DataManager.pokerResult = data.pockers;
                cc.systemEvent.emit(MINI_GAME_EVENTS.PLAY_BLINK_MINI);
                cc.systemEvent.emit(GAME_EVENTS.SHOW_JACKPOT, winData.winResult.winJP2Gold);
                // cc.systemEvent.emit(MINI_GAME_EVENTS.SHOW_MINIGAME);
            //normal win
            }else{
                console.log("normal win ======");
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.StartMiniGame);
                cc.systemEvent.emit(GAME_EVENTS.UPDATE_NORMAL_WIN_INDEX, winData.winResult.winType);
                Game.huluWin = this.checkHuluWin(winData);
                this.miniGame.setPockerList(winData.DoubleBetList);
                this.miniGame.setWinGold(winData.winResult.winGold, false);
                this.miniGame.setWinData(winData);
                DataManager.pokerResult = data.pockers;
                cc.systemEvent.emit(MINI_GAME_EVENTS.PLAY_BLINK_MINI);
            }
            // this.startMiniGame();
        }
    }

    checkHuluWin(winData: any) {
        let count = 0;
        for(let a = 0; a < winData.pockers.length; a++){
            if(winData.pockers[a] % 13 == DataManager.huluBet){
                count ++;
            }
        }
        if(count >= 3){
            return true;
        }else{
            return false;
        }
    }

    setWin(type: string) {
        switch(type){
            case WIN_TYPE.NORMAL_WIN:

                break;
            case WIN_TYPE.BONUS_WIN:

                break;
            case WIN_TYPE.MINIGAME_WIN:

                break;
            case WIN_TYPE.JP_WIN:

                break;
        }
    }

    barAniamtionCallBack(){
        this.jpAnimation.reset();
    }


    endMiniGame(){
        this.tweenNode?.stop();
        this.tweenMiniNode?.stop();
        this.tweenMiniNode
        .delay(0.4)
        .call(() => {
            this.miniGame.reset();
            this.betPannel.reset();
            this.reset();
            this.showGameByVersion();
            cc.systemEvent.emit(GAME_EVENTS.UPDATE_GAME_STATUS, GameSet.GameStatus.UnStart);
        })
        .start()
    }

    startMiniGame(){
        this.reset();
        cc.systemEvent.emit(MINI_GAME_EVENTS.SHOW_MINIGAME);
    }

    reset() {
        this.tweenMiniNode?.stop();
        this.tweenNode?.stop();
        this.nodeSport.active = false;
        this.nodeApple.active = false;
        this.nodePaMan.active = false;
        this.nodePacMan.active = false;
        this.nodePaMan.getComponent(PaManPannel).reset();
        this.nodePacMan.getComponent(PacManPannel).reset();
        this.nodeApple.getComponent(ApplePannel).reset();
        this.nodeSport.getComponent(SportPannel).reset();
        this.nodePoker.getComponent(PokerPannel).reset();
        Game.huluWin = false;
    }

    backToLobby(){
        this.reset();
        cc.director.loadScene("Load");
    }
    
    notify(data) {
        console.log("betResponse", data);
        cc.systemEvent.emit(ANI_EVENTS.HIDE_LOADING_TWEEN);
        this.setUserMoney(data.nowGold);
        if(data.winResult.winType == -1 || data.isFinish){
            this.updateUserMoneylabel();
        }
        this.winOrLose(data, data.pockers.length, data);

    }
        
  

}
