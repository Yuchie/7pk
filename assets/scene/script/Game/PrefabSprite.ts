// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const {ccclass, property} = cc._decorator;

@ccclass
export default class PrefabSprites extends cc.Component {
    
    @property(cc.Component)
    backSpriteChange: cc.Component = null;
    
    @property(cc.Component)
    NumSpriteChange: cc.Component = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad() {
        

    }
 
   
    spritePic(name){
       
        cc.resources.load(name, cc.SpriteFrame, (err, spriteFrame) => {
            console.log(name);
          //  this.backSpriteChange.getComponent(cc.Sprite).spriteFrame = spriteFrame;  
         });
    }
    
    
    spriteNum(name){
        if(!this.NumSpriteChange.node.active){
            this.NumSpriteChange.node.active = true;
        }
        if(name != "-1" && name != "ghost"){
            cc.resources.load(name, cc.SpriteFrame, (err, spriteFrame) => {
              //  this.NumSpriteChange.getComponent(cc.Sprite).spriteFrame = spriteFrame;  
             });
        }
        else{
            this.NumSpriteChange.node.active = false;
        }
       

    }
        
  

    // update (dt) {}
}
