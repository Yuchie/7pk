import FlipCardAnimation from "../Animation/FlipCardAnimation";
import ImageContainer from "../ImageContainer";
import BasePannel from "./BasePannel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PokerPannel extends BasePannel {

    start() {
       // this.reset();
    }

    showCards(cards: any) {
        cards.map(async (card , index) =>
           await this.spriteCards[index].getComponent(FlipCardAnimation).setCard(cards[index])
        )
        
        // cards.forEach((element, index) => {
        //     this.spriteCards[index].node.active = true;
        //     // this.spriteCards[index].getComponent(FlipCardAnimation).setCard(cards[index]);
        //     cards[index] == -1 ? this.spriteCards[index].spriteFrame = ImageContainer.getInstance().framePokerBackCard : this.spriteCards[index].spriteFrame = ImageContainer.getInstance().getPokerCard(cards[index]);
        // })
    } 


    reset() {
        this.spriteCards.forEach(card => {
            card.getComponent(FlipCardAnimation).reset();
        })
    }
}
