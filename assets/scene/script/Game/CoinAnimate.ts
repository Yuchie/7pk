// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import { NotifyComponent } from "../../Core/view/NotifyComponent";
import { core } from "../../Core/Core";
import { NT } from "../../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class CoinAnimate extends cc.Component {


    @property(cc.Label)
    coinLabel : cc.Label[] = [];

    @property(cc.Component)
    AllGame: cc.Component = null;

    @property(cc.Component)
    coinArrComponent: cc.Component[] = [];

    @property(cc.Component)
    jpComponent: cc.Component[]= [];

    @property(cc.Component)
    Game: cc.Component = null;

    @property(cc.Component)
    SmallGame: cc.Component = null;

   
    @property(cc.Component)
    whiteJPBar: cc.Component = null;

    @property(cc.Component)
    whiteJPBar1: cc.Component = null;
   
    private isRunning = false;
    private numlabel = 0;
    private Money = 0;
    private winJPMoney = 0;
    private isJPRunning = false;
    private JPCompNum = 0;
    private runBarCountStart = false;
    private beginCount = 0;
    private begin = 0;
    private end = 0;
  
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // this.whiteJPBar.node.active = false;
        // this.whiteJPBar1.node.active = false;
        // this.runBarCountStart = false;
        // this.beginCount = 0;

    }

    bigJPAnimate(begin, end, data){
        this.whiteJPBar.node.active = true;
        this.whiteJPBar1.node.active = true;
    

        this.begin = begin;
        this.beginCount = begin;
        this.end = end;

        this.bigJPPosition();

        var timeinterval = setInterval(()=>{
            this.beginCount++;
            if(!this.runBarCountStart){
              if(this.beginCount >= this.coinArrComponent.length){
                  this.beginCount = this.begin;
                  this.runBarCountStart = true;
              }
            }else{
                if(this.beginCount == end){
                    this.coinLabel[end].string = data.winResult.winJP2Gold;
                    clearInterval(timeinterval);
                }
            }
              this.bigJPPosition();

        }, 500);

    }
    unShowBigJP(){
        this.whiteJPBar.node.active = false;
        this.whiteJPBar1.node.active = false;
        this.runBarCountStart = false;
        this.beginCount = 0;
    }

    bigJPPosition(){
        this.whiteJPBar.node.position = this.coinArrComponent[this.beginCount].node.position;

        for(var a = 0 ; a <　this.coinArrComponent.length; a++){
            this.coinArrComponent[a].node.color = cc.color(255, 255, 255);
        }
        
        this.coinArrComponent[this.beginCount].node.color =cc.color(0,0,0);
        var pos = cc.v3(this.whiteJPBar.node.position.x+375, this.whiteJPBar.node.position.y, this.whiteJPBar.node.position.z);
        this.whiteJPBar1.node.position =  pos;
    }

   

    //初始贏錢的數字
    coinAnimate(num, winNum, times){
     //   winNum = 200;
        if(num >= 6){
            this.JPCompNum = num -6; 
            this.winJPMoney =  parseInt(this.jpComponent[this.JPCompNum].getComponent(cc.Label).string);
            this.runJpAnimate();
        }else{
            this.Game.getComponent("Game").smallGameStart();
        }

        this.numlabel = num;
        this.Money = winNum;
        this.coinLabel[this.numlabel].string = winNum;
        if(times == 1){
            this.coinArrComponent[this.numlabel].node.runAction(
                cc.repeatForever(
                    cc.blink(1, 1)
                )
            );
        }else{
            this.coinArrComponent[this.numlabel].node.stopAllActions();
            this.coinArrComponent[this.numlabel].node.opacity = 255;
        }
    }
   

    //check bet 停下閃爍
    stopBlink(){

    }

    //

    runJpAnimate(){
        this.isJPRunning = true;
    }
    //金幣滾動
    runAnimate(){
        this.isRunning = true;
    }

    stopAnimate(){

        if(this.isJPRunning){
            this.isJPRunning = false;
            if(this.winJPMoney > 0){
                this.winJPMoney = 0;
                this.jpComponent[this.JPCompNum].getComponent(cc.Label).string = "0";
                this.Game.getComponent("Game").smallGameStart();
                
            }
        }
        if(this.isRunning){
            this.isRunning = false;
            if(this.Money > 0){
                this.Money = 0
                this.coinLabel[this.numlabel].string = "-";
                //遊戲下一局開始
                this.AllGame.getComponent("AllGames").nextRound();
                this.SmallGame.getComponent("SmallGame").smallGameOver();
            }
        }

       
    }


    updateLabel(money){
        if(this.Money == 0){
            this.coinLabel[this.numlabel].string = "-";
            //遊戲下一局開始
            this.AllGame.getComponent("AllGames").nextRound();
            this.SmallGame.getComponent("SmallGame").smallGameOver();
            
        }else{
            //數字還沒接近0繼續扣
            this.coinLabel[this.numlabel].string = money+"";
        }
        
    }
    
    updateJPLabel(money){
        if(this.winJPMoney == 0){
            this.jpComponent[this.JPCompNum].getComponent(cc.Label).string = "0";
            this.Game.getComponent("Game").smallGameStart();
            
        }else{
            this.jpComponent[this.JPCompNum].getComponent(cc.Label).string = money+"";
            
        }
    }

    //用update去跑金幣滾動
    update(dt: number) {
        if(this.isRunning){
            this.updateLabel(this.Money);
            this.Money = this.Money - 10;
            if(this.Money <= 10){
                this.isRunning = false;
                this.Money = 0;
                this.updateLabel(this.Money);
            }
            // if(parseInt(this.coinLabel[this.numlabel].string) < 5){
            //     this.coinLabel[this.numlabel].string = "-";
            //     this.isRunning = false;
            // }
        }

        if(this.isJPRunning){
            this.updateJPLabel(this.winJPMoney);
            this.winJPMoney = this.winJPMoney -1;
            if(this.winJPMoney <= 10){
                this.isJPRunning = false;
                this.winJPMoney = 0;
                this.updateJPLabel(this.winJPMoney);
            }
        }


        
        



    }



 
}


