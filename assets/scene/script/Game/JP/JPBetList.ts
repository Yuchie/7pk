import { GAME_EVENTS } from "../../Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class JPBetList extends cc.Component {
    @property(cc.Label)
    JPList: cc.Label [] = [];

    public static bonusArray = [];
    private bonusDigits = "0000";
    
    start(){
        this.registerEvents();
    }

    registerEvents() {
        cc.systemEvent.on(GAME_EVENTS.UPDATE_GAME_BONUS, (data) => {
            this.regiesterNode();
            this.setBonusList(data);
        })

        cc.systemEvent.on(GAME_EVENTS.UPDATE_BONUS_NUMBER, (index, number) => {
            this.regiesterNode();
            this.updateBonusNumber(index, number);
        })

        console.log( JPBetList.bonusArray);
        if(JPBetList.bonusArray.length > 0 ){
            this.setBonusList(JPBetList.bonusArray);
        }
    }

    regiesterNode() {
        if(this.JPList == null){
            let jpNode = cc.find("Canvas/JPTableBG/JPList").children;
            let arr = [];
            jpNode.forEach((element) =>{
                arr.push(element.getComponent(cc.Label));
            })
            this.JPList = arr;
            // this.JPList = 
        }
    }

    updateBonusNumber(index, number) {
        this.JPList[index].string = this.calculateDigits(number);
        // JPBetList.bonusArray[index] = this.JPList[index];
    }

    calculateDigits(bonus: number){
        let digit = this.bonusDigits.length - bonus.toString().length;
        let digitstr = "";
        for(let i = 0; i < digit; i++){
            digitstr = digitstr+"0";            
        }
        return digitstr+bonus;
    }

    setBonusList(bonuslist: any){
        this.JPList.forEach((element, index) => {
            element.string = this.calculateDigits(bonuslist[index]);
        })
    }

    showJPList(){
        this.node.active = true;
    }

    hideJPList(){
        this.node.active = false;
    }
    
}
