
export class BaseClass{
    public static arrayJPList = 
    ["FULL BET JP:", "ROYAL FLUSH:", "FIVE OF A KIND:", "STRAIGHT FLUSH:", "FOUR OF A KIND:", "FULL HOUSE:"];

    public static JP_NAME = {
        NOTHING: 10,
        A_PAIR: 9,
        TWO_PAIR: 8,
        THREE_OF_KING: 7,
        STRAIGHT: 6,
        FLUSH: 5,
        FULL_HOUSE: 4,
        FOUR_OF_KING: 3,
        STRAIGHT_FLUSH: 2,
        FOUR_OF_KING_WITH_JOKER: 1,
        STRAIGHT_FLUSH_WITH_A: 0,
    }

    public static jpListResult(index: number){
        switch(index){
            case 10:
                return BaseClass.JP_NAME.STRAIGHT_FLUSH_WITH_A;
            case 9:
                return BaseClass.JP_NAME.FOUR_OF_KING_WITH_JOKER;
            case 8:
                return BaseClass.JP_NAME.STRAIGHT_FLUSH;
            case 7:
                return BaseClass.JP_NAME.FOUR_OF_KING;
            case 6:
                return BaseClass.JP_NAME.FULL_HOUSE;
            case 5:
                return BaseClass.JP_NAME.FLUSH;
            case 4:
                return BaseClass.JP_NAME.STRAIGHT;
            case 3:
                return BaseClass.JP_NAME.THREE_OF_KING;
            case 2:
                return BaseClass.JP_NAME.TWO_PAIR;
            case 1:
                return BaseClass.JP_NAME.A_PAIR;
            case 0:
                return BaseClass.JP_NAME.NOTHING;      
        }
    }

    public static getBonusStatus(index: number) {
        if(index >= 6){
            return true;
        }else{
            return false;
        }
    }

    public static getBetListByIndex(index: number) {
        switch(index){
            case 9:
                return 1;
            case 8:
                return 1;
            case 7:
                return 2;
            case 6:
                return 3;
            case 5:
                return 5;
            case 4:
                return 7;
            case 3:
                return 50;
            case 2:
                return 120;
            case 1:
                return 200;
            case 0:
                return 500;      
        }
    }
}

// export class BET_RESULT_ENUM {
//     ROYAL_FLUSH = 500
//     FIVE_KIND = 200
//     STR_FLUSH = 120
//     FOUR_KIND = 50
//     FULL_HOUSE = 7
//     FLUSH = 5
//     STRAIGHT = 3
//     THREE_KIND = 2
//     TWO_PAIR = 1
//     ACE_PAIR = 1
// }
