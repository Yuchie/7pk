const {ccclass, property} = cc._decorator;
@ccclass
export default class zoomInOut extends cc.Component {

    @property(cc.Button)
    btn : cc.Button = null;

    private btnSize = 0;

    onLoad () {
        
        this.btnSize = this.btn.node.scale;
    }

    zoom_in_out(){
        cc.tween(this.btn.node)
        .to(0.1, {scale: this.btnSize+0.1})
        .to(0.1, {scale: this.btnSize})
        .start()
        
    }


    

  



 
}
