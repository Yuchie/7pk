import { ANI_EVENTS, ANI_NAME } from "./Utils/Event";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LoadData extends cc.Component {
    @property(cc.Label)
    labelL: cc.Label = null;

    @property(cc.Label)
    labelO: cc.Label = null;

    @property(cc.Label)
    labelA: cc.Label = null;

    @property(cc.Label)
    labelD: cc.Label = null;

    @property(cc.Animation)
    aniLabel: cc.Animation = null;
    
    onLoad() {
        this.registerEvents();
        this.node.active = false;
    }

    registerEvents() {
        cc.systemEvent.on(ANI_EVENTS.SHOW_LOADING_TWEEN, ()=>{
            console.log("ANI_EVENTS.SHOW_LOADING_TWEEN");
            this.show();
        })

        cc.systemEvent.on(ANI_EVENTS.HIDE_LOADING_TWEEN, ()=>{
            console.log("ANI_EVENTS.HIDE_LOADING_TWEEN");
            this.hide();
        })
    }

    show() {
        if(this.node)
            this.node.active = true;
            this.playLoadAni();
    }
    
    playLoadAni() {
        if(this.node){
            this.aniLabel.play(ANI_NAME.LOADING_LABEL_TWEEN);
        }
    }

    hide() {
        if(this.node)
        this.node.active = false;
    }



}
