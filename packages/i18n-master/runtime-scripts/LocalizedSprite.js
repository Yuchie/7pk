const SpriteFrameSet = require('SpriteFrameSet');
cc.Class({
    extends: cc.Component,

    /*editor: {
        executeInEditMode: true,
        inspector: 'packages://i18n/inspector/localized-sprite.js',
        menu: 'i18n/LocalizedSprite'
    },*/

    properties: {
        spriteFrameSet: {
            default: [],
            type: SpriteFrameSet
        },
    },

    onLoad () {
        if(!CC_EDITOR) {
            this.fetchRender();
        }
    },

    fetchRender () {
        let sprite = this.getComponent(cc.Sprite);
        if (sprite) {
            this.sprite = sprite;
            this.updateSprite(window.i18n.curLang);
            return;
        }
    },

    getSpriteFrameByLang (lang) {
        for (let i = 0; i < this.spriteFrameSet.length; ++i) {
            if (this.spriteFrameSet[i].language === lang) {
                var path = this.spriteFrameSet[i].spriteFramePath
                cc.resources.load(path, cc.SpriteFrame, (err, asset) => {
                    //this.getComponent(cc.Sprite).spriteFrame = asset;
                    this.sprite.spriteFrame = asset;
                   // return asset;
                });
                //return this.spriteFrameSet[i].spriteFrame;
            }
        }
    },

    updateSprite (language) {
        if (!this.sprite) {
            //cc.error('Failed to update localized sprite, sprite component is invalid!');
            return;
        }

        this.getSpriteFrameByLang(language);
        /*
       有誤
       let spriteFrame = this.getSpriteFrameByLang(language);

       if (!spriteFrame && this.spriteFrameSet[0]) {
           spriteFrame = this.spriteFrameSet[0].spriteFrame;
       }
       */

       // this.sprite.spriteFrame = spriteFrame;
    }
});