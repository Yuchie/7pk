const SpriteFrameSet = cc.Class({
    name: 'SpriteFrameSet',
    properties: {
        language: '',
        spriteFramePath:""
    }
});

module.exports = SpriteFrameSet;